$(function () {
    $('.js-basic-example').DataTable({
        responsive: true,
        language: {
            infoEmpty: "No records available - Got it?",
        }
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});