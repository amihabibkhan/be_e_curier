@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Make a new {{ ucfirst($type) }}</h2>
                </div>
                <div class="body">
                    <form method="POST" action="{{ route('userRegistration.store') }}" enctype="multipart/form-data">
                        @if($type == "merchant")
                            <input type="hidden" value="3" name="role_id">
                        @elseif($type == "delivery-boy")
                            <input type="hidden" value="4" name="role_id">
                        @elseif($type == "manager")
                            <input type="hidden" value="2" name="role_id">
                        @endif

                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">Name</label>
                                    <input type="text" value="{{ old('name') }}" class="form-control" placeholder="Name" name="name">
                                    @if($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Profile Picture (200x200)</label>
                                    <input type="file" class="form-control" name="profile_pic">
                                </div>
                                @if($type == "merchant")
                                    <div class="mb-20">
                                        <label for="">Post / Designation</label>
                                        <input type="text" value="{{ old('designation') }}" class="form-control" placeholder="Post or Designation" name="designation">
                                    </div>
                                @endif
                                <div class="mb-20">
                                    <label for="">E-mail</label>
                                    <input type="email" class="form-control" value="{{ old('email') }}" placeholder="E-mail Address" name="email">
                                    @if($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" placeholder="Create a new strong password" name="password">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Create a new strong password" name="password_confirmation">
                                    @if($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Phone Number</label>
                                    <input type="text" class="form-control" value="{{ old('phone') }}" placeholder="Phone Number" name="phone">
                                    @if($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- merchant start --}}
                                @if($type == "merchant")
                                    <div class="mb-20">
                                        <label for="">Company Name</label>
                                        <input type="text" class="form-control" value="{{ old('company_name') }}" placeholder="Company Name" name="company_name">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">Company Facebook Fan Page</label>
                                        <input type="text" class="form-control" value="{{ old('fb_fan_page') }}" placeholder="Facebook Fan Page Link" name="fb_fan_page">
                                    </div>
                                @endif
                                {{-- merchant end --}}

                                {{-- delivery boy start --}}
                                @if($type == 'delivery-boy')
                                    <div class="mb-20">
                                        <label for="">NID/Birth Certificate Scan Copy</label>
                                        <input type="file" class="form-control" name="nid_image">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">NID/Birth Certificate Number</label>
                                        <input type="text" class="form-control" value="{{ old('nid_number') }}" placeholder="NID or Birth Certificate Number" name="nid_number">
                                    </div>
                                @endif
                                {{-- delivery boy end --}}
                            </div>
                            <div class="col-md-6">

                                {{-- merchant start --}}
                                @if($type == "merchant")
                                    <div class="mb-20">
                                        <label for="">Company Website</label>
                                        <input type="text" value="{{ old('website') }}" class="form-control" placeholder="Company Website Link" name="website">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">Company Logo</label>
                                        <input type="file" class="form-control" name="company_logo">
                                    </div>
                                @endif
                                {{-- merchant end --}}

                                <div class="mb-20">
                                    <label for="">Branch</label>
                                    <select name="branch_id" class="form-control">
                                        <option disabled selected>-- Select a Branch --</option>
                                        @foreach($branches as $branch)
                                            <option value="{{ $branch->id }}" {{ ($branch->id == old('branch_id')) ? 'selected' : '' }}>{{ $branch->branch_name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('branch_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('branch_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Division</label>
                                    <select name="division_id"  id="division" onchange="getDistrict()"  class="form-control">
                                        <option disabled selected>-- Select a Division --</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id }}" {{ ($division->id == old('division_id')) ? 'selected' : '' }}>{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">District</label>
                                    <select name="district_id" onchange="getUpazila()" class="form-control" id="district">
                                        <option disabled selected>Select a District</option>
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Upazila</label>
                                    <select name="upazila_id" class="form-control" id="upazila">
                                        <option disabled selected>Select an Upazila</option>
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Area</label>
                                    <select name="area_id" class="form-control">
                                        <option disabled selected>Select an Area </option>
                                        @foreach($areas as $area)
                                            <option value="{{ $area->id }}" {{ ($area->id == old('area_id')) ? 'selected' : '' }}>{{ $area->area_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Present Address</label>
                                    <input type="text" class="form-control" value="{{ old('present_address') }}" placeholder="Present Address" name="present_address">
                                </div>
                                {{-- delivery boy start --}}
                                @if($type == 'delivery-boy')
                                    <div class="mb-20">
                                        <label for="">Permanent Address</label>
                                        <input type="text" class="form-control" placeholder="Permanent Address" value="{{ old('parmanent_address') }}" name="parmanent_address">
                                    </div>
                                @endif
                                {{-- delivery boy end --}}
                               <div class="text-right">
                                   <input type="submit" class="btn btn-success btn-lg" value="Create User">
                               </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    <script>
        function getDistrict() {
            var division_id = document.getElementById('division').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/district',
                data: {division_id:division_id},
                success: function (data) {
                    document.getElementById('district').innerHTML = data;
                }
            });
        }

        function getUpazila() {
            var district_id = document.getElementById('district').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/upazila',
                data: {district_id:district_id},
                success: function (data) {
                    document.getElementById('upazila').innerHTML = data;
                }
            });
        }
    </script>
@endsection
