
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To Admin</title>
    <!-- Favicon-->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('admin') }}/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('admin') }}/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('admin') }}/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('admin') }}/plugins/morrisjs/morris.css" rel="stylesheet" />

    {{-- css yield --}}
    @yield("css")

    <!-- Custom Css -->
    <link href="{{ asset('admin') }}/css/style.css" rel="stylesheet">
    <link href="{{ asset('admin') }}/css/my_css.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('admin') }}/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ route('home') }}">LEGEND ADMIN</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">notifications</i>
                        <span class="label-count">{{ (count($notifications) > 10) ? "10+" : count($notifications) }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu">
                                @foreach($notifications as $notification)
                                    <li>
                                        <a href="{{ route('notifications') }}">
                                            <div class="menu-info">
                                                <h4>{{ $notification->type }}</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> {{ $notification->created_at->diffForHumans() }}
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    @if($loop->index == 7)
                                        @break
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="{{ route('notifications') }}">View All Notifications</a>
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                @if(auth::user()->profile_pic && auth::check())
                    <img src="{{ asset('storage') }}/{{ auth::user()->profile_pic }}" width="48" height="48" alt="User" />
                @else
                    <img src="{{ asset('admin') }}/images/user.png" width="48" height="48" alt="User" />
                @endif
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ auth::user()->name }}</div>
                <div class="email">{{ auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{ route('personalProfile') }}"><i class="material-icons">person</i>Profile</a></li>
                        <li><a href="{{ route('updateProfile') }}"><i class="material-icons">mode_edit</i>Update</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>Sign Out
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="active">
                    <a href="{{ route('home') }}">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                @merchant
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings_input_antenna</i>
                        <span>Pick Orders</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('order_request.create') }}"><span>Request a new order</span></a></li>
                        <li><a href="{{ route('order_request.show', 'pending') }}"><span>Pending Order List</span></a></li>
                        <li><a href="{{ route('order_request.show', 'processing') }}"><span>Processing Order List</span></a></li>
                        <li><a href="{{ route('order_request.show', 'completed') }}"><span>Completed Order List</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">directions_bike</i>
                        <span>Delivery</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('merchantOrderList', 'all') }}"><span>Delivery Processing</span></a></li>
                        <li><a href="{{ route('merchantOrderList', 'completed') }}"><span>Delivery Completed</span></a></li>
                        <li><a href="{{ route('merchantOrderList', 'pending-for-return') }}"><span>Pending for Return</span></a></li>
                        <li><a href="{{ route('merchantOrderList', 'cancelled') }}"><span>Cancelled Order list</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">attach_money</i>
                        <span>Payments</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('merchantPaymentList') }}"><span>Income Histories</span></a></li>
                        <li><a href="{{ route('merchantDeliveryFeePaymentHistory') }}"><span>Payment Histories</span></a></li>
                    </ul>
                </li>
                @endmerchant
                @manager
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">account_circle</i>
                        <span>Users</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>User List</span>
                            </a>
                            <ul class="ml-menu">
                                <li><a href="{{ route('adminUser.list', 'manager') }}">Manager</a></li>
                                <li><a href="{{ route('adminUser.list', 'merchant') }}">Merchant User</a></li>
                                <li><a href="{{ route('adminUser.list', 'delivery-boy') }}">Devlivery Boy</a></li>
                                <li><a href="{{ route('adminUser.list', 'pending') }}">Pending for Approval</a></li>
                                <li><a href="{{ route('adminUser.list', 'suspended') }}">Suspended Users</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Create User</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{ route('registrationForm', 'manager') }}">Create Branch Manager</a>
                                </li>
                                <li>
                                    <a href="{{ route('registrationForm', 'merchant') }}">Create Merchant User</a>
                                </li>
                                <li>
                                    <a href="{{ route('registrationForm', 'delivery-boy') }}">Create Delivery Boy</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                {{-- user menu end --}}

                {{-- order menu start --}}
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings_input_antenna</i>
                        <span>Pick Orders</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('order_request.create') }}"><span>Request a new order</span></a></li>
                        <li><a href="{{ route('orderListForAdmin', 'pending') }}"><span>Pending Order Request</span></a></li>
                        <li><a href="{{ route('orderListForAdmin', 'approved') }}"><span>Approved Order List</span></a></li>
                        <li><a href="{{ route('orderListForAdmin', 'processing') }}"><span>Processing Order List</span></a></li>
                        <li><a href="{{ route('orderListForAdmin', 'completed') }}"><span>Completed Order List</span></a></li>
                    </ul>
                </li>
                {{-- order menu end --}}

                {{-- Delivery start --}}
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">directions_bike</i>
                        <span>Delivery</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('deliveryListForAdmin', 'pending-for-send') }}"><span>Pending for Send</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'pending-to-receive') }}"><span>Pending to Receive</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'arrived-to-office') }}"><span>Arrived to office</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'taken-by-boy') }}"><span>Taken by delivery boy</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'on-the-way') }}"><span>On the way</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'cancelled') }}"><span>Cancelled Order List</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'requested-for-complete') }}"><span>Requested For complete</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'completed') }}"><span>Delivery Completed</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'requested-to-returned') }}"><span>Requested to Return</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'returned-to-office') }}"><span>Returned to office</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'send-to-merchant') }}"><span>Sent Back  to Merchant</span></a></li>
                        <li><a href="{{ route('deliveryListForAdmin', 'received-by-merchant') }}"><span>Received by Merchant</span></a></li>
                    </ul>
                </li>
                {{-- Delivery end --}}

                {{-- payment start --}}

                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">attach_money</i>
                        <span>Payments</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('adminPaymentHistory') }}"><span>Delivery Boy Payment</span></a></li>
                        <li><a href="{{ route('merchantPaymentHistory') }}"><span>Merchant Payment</span></a></li>
                        <li><a href="{{ route('merchantFeeHistory') }}"><span>Merchant Delivery Fee</span></a></li>
                    </ul>
                </li>
                {{-- payment end --}}

                @endmanager
                @admin
                <li>
                    <a href="{{ route('branch.index') }}">
                        <i class="material-icons">more</i>
                        <span>Branch </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('area.index') }}">
                        <i class="material-icons">location_on</i>
                        <span>Area List</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('delivery_system.index') }}">
                        <i class="material-icons">directions_run</i>
                        <span>Delivery System</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cancel_reason.index') }}">
                        <i class="material-icons">cancel</i>
                        <span>Cancel Reason</span>
                    </a>
                </li>
                @endadmin
                @boy
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings_input_antenna</i>
                        <span>Pick Orders</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('boy.orderList', 'pending') }}"><span>Pending Order List</span></a></li>
                        <li><a href="{{ route('boy.orderList', 'accepted') }}"><span>Accepted Order List</span></a></li>
                        <li><a href="{{ route('boy.orderList', 'completed') }}"><span>Completed Order List</span></a></li>
                        <li><a href="{{ route('boy.orderList', 'rejected') }}"><span>Rejected Order List</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">directions_bike</i>
                        <span>Delivery Orders</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('boyDeliveryList', 'pending') }}"><span>Pending Order List</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'accepted') }}"><span>Accepted Order List</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'way') }}"><span>On the way</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'complete-pending') }}"><span>Pending for complete</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'completed') }}"><span>Completed Order List</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'cancelled') }}"><span>Cancelled Order List</span></a></li>
                        <li><a href="{{ route('boyDeliveryList', 'rejected') }}"><span>Rejected Order List</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">attach_money</i>
                        <span>Income &amp; Payment</span>
                    </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('boyIncomeHistory') }}"><span>Income Histories</span></a></li>
                        <li><a href="{{ route('boyPaymentHistory') }}"><span>Payment Histories</span></a></li>
                    </ul>
                </li>
                @endboy
                <li>
                    <a href="{{ route('notifications') }}">
                        <i class="material-icons">notifications</i>
                        <span>Notifications</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; {{ date("Y")-1 }} - {{ date("Y") }} <a href="http://legenditinstitute.com" target="_blank">Legend IT Institute</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        @if(session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('error') }}
            </div>
        @endif
        @yield("main_content")
    </div>
</section>

<!-- Jquery Core Js -->
<script src="{{ asset('admin') }}/plugins/jquery/jquery.min.js"></script>


<!-- Bootstrap Core Js -->
<script src="{{ asset('admin') }}/plugins/bootstrap/js/bootstrap.js"></script>

@yield("javascript")

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('admin') }}/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('admin') }}/plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{{ asset('admin') }}/plugins/jquery-countto/jquery.countTo.js"></script>




<!-- Custom Js -->
<script src="{{ asset('admin') }}/js/admin.js"></script>
<script src="{{ asset('admin') }}/js/pages/index.js"></script>

<!-- Demo Js -->
<script src="{{ asset('admin') }}/js/demo.js"></script>

</body>

</html>
