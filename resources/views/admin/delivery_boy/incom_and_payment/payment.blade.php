@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2 style="margin-bottom: 15px">
                        Payment History
                    </h2>
                    <h2>(Total Paid - {{ $total_payment }}/- TK)</h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <style>
                            .pending{
                                padding: 5px 10px;
                                background-color: #9f105c;
                                color: #fff;
                            }
                            .success{
                                padding: 5px 10px;
                                background-color: #0d95e8;
                                color: #fff;
                            }
                            .rejected{
                                padding: 5px 10px;
                                background-color: red;
                                color: #fff;
                            }
                        </style>
                        @if(count($payments) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Paid By</th>
                                    <th class="text-center">Amount</th>
                                    <th>Payment Status</th>
                                    <th>Note</th>
                                    <th>Paid At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->user->name }}</td>
                                        <td class="text-center">{{ $payment->amount }} /-</td>
                                        <td>
                                            @if($payment->status == 0)
                                                <span class='pending'>Pending</span>
                                            @elseif($payment->status == 1)
                                                <span class='success'>Success</span>
                                            @else
                                                <span class='rejected'>Rejected</span>
                                            @endif
                                        </td>
                                        <td title="{{ $payment->note }}">{{ str_limit($payment->note, 30) }}</td>
                                        <td class="text-center">{{ $payment->created_at->diffForHumans() }}</td>
                                        <td>
                                            @if($payment->status == 0)
                                                <button type="button" class="btn bg-green" data-toggle="modal" data-target="#accept{{ $payment->id }}">Accept</button>

                                                <div style="white-space: normal" class="modal fade" id="accept{{ $payment->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content modal-col-red">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Accept Payment Request</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure to Accept?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('acceptPaymentRequest', ['accept', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn bg-red" data-toggle="modal" data-target="#reject{{ $payment->id }}">Reject</button>

                                                <div style="white-space: normal" class="modal fade" id="reject{{ $payment->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content modal-col-red">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Reject Payment Request</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure to Reject?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('acceptPaymentRequest', ['reject', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection