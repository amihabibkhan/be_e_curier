@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>
                        List of {{ ucfirst($type) }} Orders
                    </h2>
                </div>
                <style>
                    ._1{
                        padding: 5px 10px; background-color: #1b4b72; color: #fff
                    }
                    ._2{
                        padding: 5px 10px; background-color: #9f105c; color: #fff
                    }
                    ._3{
                        padding: 5px 10px; background-color: #4a148c; color: #fff
                    }
                    ._4{
                        padding: 5px 10px; background-color: #0d47a1; color: #fff
                    }
                    ._5{
                        padding: 5px 10px; background-color: #5b32bc; color: #fff;
                    }
                    ._6{
                        padding: 5px 10px; background-color: #7ecff4; color: #000;
                    }
                    ._7{
                        padding: 5px 10px; background-color: #811b1b; color: #fff;
                    }
                    ._8{
                        padding: 5px 10px; background-color: black; color: #fff;
                    }
                    ._9{
                        padding: 5px 10px; background-color: red; color: #fff;
                    }
                    ._10{
                        padding: 5px 10px; background-color: #9C27B0; color: #fff;
                    }
                    ._11{
                        padding: 5px 10px; background-color: #0f74a8; color: #fff;
                    }
                    ._12{
                        padding: 5px 10px; background-color: #1b1e21; color: #fff;
                    }
                    ._13{
                        padding: 5px 10px; background-color: #0000ee; color: #fff;
                    }
                </style>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($products) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Order ID</th>
                                    <th>Receiving Branch</th>
                                    <th>Receiver</th>
                                    <th>Receiver Phone</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <?php
                                    switch ($product->order_status_id){
                                        case 1: $class = "_1";
                                            break;
                                        case 2: $class = "_2";
                                            break;
                                        case 3: $class = "_3";
                                            break;
                                        case 4: $class = "_4";
                                            break;
                                        case 5: $class = "_5";
                                            break;
                                        case 6: $class = "_6";
                                            break;
                                        case 7: $class = "_7";
                                            break;
                                        case 8: $class = "_8";
                                            break;
                                        case 9: $class = "_9";
                                            break;
                                        case 10: $class = "_10";
                                            break;
                                        case 11: $class = "_11";
                                            break;
                                        case 12: $class = "_12";
                                            break;
                                        case 13: $class = "_13";
                                            break;
                                    }
                                    ?>
                                    <tr>
                                        <td>{{ $product->tracking_id }}</td>
                                        <td>{{ $product->order->tracking_id }}</td>
                                        <td>{{ $product->toBranch->branch_name }}</td>
                                        <td>{{ $product->receiver_name }}</td>
                                        <td>{{ $product->receiver_phone }}</td>
                                        <td class="text-center"><span class="{{ $class }}">{{ $product->order_status->status_title }}</span></td>
                                        <td class="text-center">{{ $product->created_at->diffForHumans() }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            {{-- view start --}}
                                            <button type="button" class="btn bg-green" data-toggle="modal" data-target="#view{{ $product->id }}">View</button>
                                            <div style="white-space: normal" class="modal fade" id="view{{ $product->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Product Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="table-responsive">
                                                                <style>
                                                                    .left_style{
                                                                        background-color: #9f105c;
                                                                        color: #fff;
                                                                    }
                                                                    .right_style{
                                                                        background-color: #5b32bc;
                                                                        color: #fff;
                                                                    }
                                                                </style>
                                                                <table class="table table-bordered table-striped table-hover">
                                                                    <tr>
                                                                        <th class="left_style">Product ID</th>
                                                                        <td class="left_style">{{ $product->tracking_id }}</td>
                                                                        <th class="right_style">Order ID</th>
                                                                        <td class="right_style">{{ $product->order->tracking_id }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">From Branch</th>
                                                                        <td class="left_style">{{ $product->fromBranch->branch_name }}</td>
                                                                        <th class="right_style">To Branch</th>
                                                                        <td class="right_style">{{ $product->toBranch->branch_name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Product Description</th>
                                                                        <td colspan="3" class="left_style">{{ $product->product_description }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Receiver Name</th>
                                                                        <td class="left_style">{{ $product->receiver_name }}</td>
                                                                        <th class="right_style">Receiver Phone</th>
                                                                        <td class="right_style">{{ $product->receiver_phone }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Address</th>
                                                                        <td class="left_style" colspan="3">{{ $product->area->area_name }}, {{ $product->upazila->name }}, {{ $product->district->name }}, {{ $product->division->name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Order Status</th>
                                                                        <td class="left_style">{{ $product->order_status->status_title }}</td>
                                                                        <th class="right_style">Total Amount</th>
                                                                        <td class="right_style">{{ $product->total_amount }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Delivery System</th>
                                                                        <td colspan="3" class="left_style">{{ $product->delivery_system->system_details }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Delivery Payment System</th>
                                                                        <td class="left_style" colspan="3">{{ $product->delivery_payment_system->system_title }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- view end --}}
                                            {{-- accept and reject start --}}
                                            @if($product->boy_order->boy_order_status_id ==1)
                                                <button type="button" class="btn bg-brown" data-toggle="modal" data-target="#accept{{ $product->id }}">Accept</button>
                                                <div style="white-space: normal" class="modal fade" id="accept{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content text-center modal-col-brown">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>Are you sure to Accept?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('acceptDeliveryRequest', $product->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn bg-red" data-toggle="modal" data-target="#reject{{ $product->id }}">Reject</button>
                                                <div style="white-space: normal" class="modal fade" id="reject{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content text-center modal-col-red">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>Are you sure to Reject?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('rejectDeliveryRequest', $product->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{-- accept and reject end --}}
                                            {{-- collect product start --}}
                                            @if($product->boy_order->boy_order_status_id ==2)
                                                <button type="button" class="btn bg-brown" data-toggle="modal" data-target="#collect{{ $product->id }}">Collect Product</button>
                                                <div style="white-space: normal" class="modal fade" id="collect{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content text-center modal-col-brown">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>Have you got the Product from office?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('collectProductForDelivery', $product->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{-- collect product end --}}
                                            {{-- confirm delivery start --}}
                                            @if($product->boy_order->boy_order_status_id == 6)
                                                <button type="button" class="btn bg-brown" data-toggle="modal" data-target="#complete{{ $product->id }}">Complete Delivery</button>
                                                <div style="white-space: normal" class="modal fade" id="complete{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content text-center modal-col-brown">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>Delivery completed?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('requestToCompleteDelivery', $product->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn bg-red" data-toggle="modal" data-target="#cancel{{ $product->id }}">Cancel</button>
                                                <div style="white-space: normal" class="modal fade" id="cancel{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center" role="document">
                                                        <div class="modal-content text-center modal-col-brown">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <form action="{{ route('cancelOrderByBoy') }}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="order_id" value="{{ $product->id }}">
                                                                <div class="modal-body text-left">
                                                                    <div class="mb-20">
                                                                        <label for="">Cancel Reason</label>
                                                                        <select name="reason_id" style="width: 100%" class="form-control">
                                                                            <option value="">-- Select a cancel reason --</option>
                                                                            @foreach($cancels as $cancel)
                                                                                <option value="{{ $cancel->id }}">{{ $cancel->reason_title }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Cancel</button>
                                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{-- confirm delivery end --}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection