@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>
                        List of {{ ucfirst($type) }} Orders
                    </h2>
                </div>
                <style>
                    ._1{
                        padding: 5px 10px; background-color: #1b4b72; color: #fff
                    }
                    ._2{
                        padding: 5px 10px; background-color: #9f105c; color: #fff
                    }
                    ._3{
                        padding: 5px 10px; background-color: #4a148c; color: #fff
                    }
                    ._4{
                        padding: 5px 10px; background-color: #0d47a1; color: #fff
                    }
                    ._5{
                        padding: 5px 10px; background-color: green; color: #fff;
                    }
                </style>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($orders) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Company Name</th>
                                    <th>Phone</th>
                                    <th>Product Quantity</th>
                                    <th>Weight</th>
                                    <th>Order Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                        <?php
                                            switch ($order->main_order_status_id){
                                                case 1: $class = "_1";
                                                break;
                                                case 2: $class = "_2";
                                                break;
                                                case 3: $class = "_3";
                                                break;
                                                case 4: $class = "_4";
                                                break;
                                                case 5: $class = "_5";
                                                break;
                                            }
                                        ?>
                                    <tr>
                                        <td>{{ $order->tracking_id }}</td>
                                        <td><a href="{{ route('singleUserView', $order->user_id) }}">{{ $order->user->company_name }}</a></td>
                                        <td>{{ $order->user->phone }}</td>
                                        <td class="text-center">{{ $order->product_quantity }}</td>
                                        <td class="text-center">{{ $order->weight }}</td>
                                        <td class="text-center"><span class="{{ $class }}">{{ $order->main_order_status->status_title }}</span></td>
                                        <td class="text-center">{{ $order->created_at->diffForHumans() }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            @if($order->main_order_status_id != 1)
                                                <a href="{{ route('boy.orderDetails', $order->id) }}" class="btn bg-black">Details</a>
                                            @endif

                                            @if($order->picker_assign->boy_order_status_id == 2)
                                                <a href="{{ route('boy.addProductsToOrder', $order->id) }}" class="btn btn-success">Pick Products</a>
                                            @endif


                                            @if($order->picker_assign->boy_order_status_id == 1)
                                                {{-- accept request start --}}
                                                    <button type="button" class="btn bg-cyan" data-toggle="modal" data-target="#accept{{ $order->id }}">Accept</button>

                                                    <div style="white-space: normal" class="modal fade" id="accept{{ $order->id }}" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                            <div class="modal-content modal-col-cyan">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="smallModalLabel">Accept Order Request</h4>
                                                                </div>
                                                                    <div class="modal-body">
                                                                         Are you sure to Accept?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <a href="{{ route('boy.acceptPickOrder', ['accept', $order->id]) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {{-- accept request end --}}
                                                {{-- reject request start --}}
                                                    <button type="button" class="btn bg-red" data-toggle="modal" data-target="#reject{{ $order->id }}">Reject</button>

                                                    <div style="white-space: normal" class="modal fade" id="reject{{ $order->id }}" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                            <div class="modal-content modal-col-red">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="smallModalLabel">Reject Order Request</h4>
                                                                </div>
                                                                    <div class="modal-body">
                                                                         Are you sure to Reject?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <a href="{{ route('boy.acceptPickOrder', ['reject', $order->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {{-- reject request end --}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection