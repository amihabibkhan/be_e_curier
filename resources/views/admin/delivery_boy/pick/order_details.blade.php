@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Order Details ({{ $order_info->tracking_id }})
                    </h2>

                    @boy
                        <a href="{{ route('boy.addProductsToOrder', $order_info->id) }}" class="header-dropdown m-r--5 btn btn-success">Add a New Product</a>
                    @endboy
                </div>
                <style>
                    ._1{
                        padding: 5px 10px; background-color: #1b4b72; color: #fff
                    }
                    ._2{
                        padding: 5px 10px; background-color: #9f105c; color: #fff
                    }
                    ._3{
                        padding: 5px 10px; background-color: #4a148c; color: #fff
                    }
                    ._4{
                        padding: 5px 10px; background-color: #0d47a1; color: #fff
                    }
                    ._5{
                        padding: 5px 10px; background-color: green; color: #fff;
                    }
                </style>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($products) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Order ID</th>
                                    <th>Receiving Branch</th>
                                    <th>Receiver</th>
                                    <th>Receiver Phone</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        <?php
                                            switch ($product->order_status_id){
                                                case 1: $class = "_1";
                                                break;
                                                case 2: $class = "_2";
                                                break;
                                                case 3: $class = "_3";
                                                break;
                                                case 4: $class = "_4";
                                                break;
                                                case 5: $class = "_5";
                                                break;
                                            }
                                        ?>
                                    <tr>
                                        <td>{{ $product->tracking_id }}</td>
                                        <td>{{ $product->order->tracking_id }}</td>
                                        <td>{{ $product->toBranch->branch_name }}</td>
                                        <td>{{ $product->receiver_name }}</td>
                                        <td>{{ $product->receiver_phone }}</td>
                                        <td class="text-center"><span class="{{ $class }}">{{ $product->order_status->status_title }}</span></td>
                                        <td class="text-center">{{ $product->created_at->diffForHumans() }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            {{-- view start --}}
                                            <button type="button" class="btn bg-green" data-toggle="modal" data-target="#view{{ $product->id }}">View</button>
                                            <div style="white-space: normal" class="modal fade" id="view{{ $product->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Product Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="table-responsive">
                                                                <style>
                                                                    .left_style{
                                                                        background-color: #9f105c;
                                                                        color: #fff;
                                                                    }
                                                                    .right_style{
                                                                        background-color: #5b32bc;
                                                                        color: #fff;
                                                                    }
                                                                </style>
                                                                <table class="table table-bordered table-striped table-hover">
                                                                    <tr>
                                                                        <th class="left_style">Product ID</th>
                                                                        <td class="left_style">{{ $product->tracking_id }}</td>
                                                                        <th class="right_style">Order ID</th>
                                                                        <td class="right_style">{{ $product->order->tracking_id }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">From Branch</th>
                                                                        <td class="left_style">{{ $product->fromBranch->branch_name }}</td>
                                                                        <th class="right_style">To Branch</th>
                                                                        <td class="right_style">{{ $product->toBranch->branch_name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Product Description</th>
                                                                        <td colspan="3" class="left_style">{{ $product->product_description }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Receiver Name</th>
                                                                        <td class="left_style">{{ $product->receiver_name }}</td>
                                                                        <th class="right_style">Receiver Phone</th>
                                                                        <td class="right_style">{{ $product->receiver_phone }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Address</th>
                                                                        <td class="left_style" colspan="3">{{ $product->area->area_name }}, {{ $product->upazila->name }}, {{ $product->district->name }}, {{ $product->division->name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Order Status</th>
                                                                        <td class="left_style">{{ $product->order_status->status_title }}</td>
                                                                        <th class="right_style">Total Amount</th>
                                                                        <td class="right_style">{{ $product->total_amount }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Delivery System</th>
                                                                        <td class="left_style">{{ $product->delivery_system->system_details }}</td>
                                                                        <th class="right_style">Delivery Fee Status</th>
                                                                        <td class="right_style">{{ ($product->delivery_fee_payment_status == 0) ? "Unpaid" : "Paid"  }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="left_style">Delivery Payment System</th>
                                                                        <td class="left_style" colspan="3">{{ $product->delivery_payment_system->system_title }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- view end --}}
                                            {{-- edit start --}}
                                            @if($product->order_status_id == 1 && auth::user()->role_id != 3)
                                            <button type="button" class="btn bg-red" data-toggle="modal" data-target="#delete{{ $product->id }}">Delete</button>
                                            <div style="white-space: normal" class="modal fade" id="delete{{ $product->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                    <div class="modal-content modal-col-red">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are you sure to delete?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            {{-- Delete end --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection