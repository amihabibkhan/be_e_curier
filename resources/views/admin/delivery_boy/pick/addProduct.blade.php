@extends('layouts.adminlayout')

@section("main_content")
    <style>
        .d-block{
            display: block;
        }
        .d-none{
            display: none;
        }
    </style>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Add a product to order</h2>
                </div>
                <div class="body">
                    <form action="{{ route('boy.addProductsToOrderSubmit') }}" method="post">
                        @csrf
                        <input type="hidden" name="order_id" value="{{ $order_id }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">Receiver Name</label>
                                    <input type="text" class="form-control" value="{{ old('receiver_name') }}" name="receiver_name" placeholder="Receiver Name">
                                    @if($errors->has('receiver_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('receiver_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Receiver Phone</label>
                                    <input type="text" class="form-control" value="{{ old('receiver_phone') }}" name="receiver_phone" placeholder="Receiver Phone">
                                    @if($errors->has('receiver_phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('receiver_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Division</label>
                                    <select name="division_id"  id="division" onchange="getDistrict()"  class="form-control">
                                        <option disabled selected>-- Select a Division --</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id }}" {{ ($division->id == old('division_id')) ? 'selected' : '' }}>{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('division_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('division_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">District</label>
                                    <select name="district_id" onchange="getUpazila()" class="form-control" id="district">
                                        <option disabled selected>Select a District</option>
                                    </select>
                                    @if($errors->has('district_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('district_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Upazila</label>
                                    <select name="upazila_id" class="form-control" id="upazila">
                                        <option disabled selected>Select an Upazila</option>
                                    </select>
                                    @if($errors->has('upazila_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('upazila_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Branch</label>
                                    <select name="to_branch" class="form-control">
                                        <option disabled selected>Select an Area </option>
                                        @foreach($branches as $branch)
                                            <option value="{{ $branch->id }}" {{ ($branch->id == old('to_branch')) ? 'selected' : '' }}>{{ $branch->branch_name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('to_branch'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('to_branch') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">Area</label>
                                    <select name="area_id" class="form-control">
                                        <option disabled selected>Select an Area </option>
                                        @foreach($areas as $area)
                                            <option value="{{ $area->id }}" {{ ($area->id == old('area_id')) ? 'selected' : '' }}>{{ $area->area_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Product Information</label>
                                    <input type="text" class="form-control" value="{{ old('product_description') }}" name="product_description" placeholder="ex: Book, Watch, Pen etc">
                                </div>
                                <div class="mb-20">
                                    <label for="">Total Amount</label>
                                    <input type="text" class="form-control" value="{{ old('total_amount') }}" name="total_amount" placeholder="Total amount which receiver have to pay">
                                </div>
                                <div class="mb-20">
                                    <label for="">Delivery System</label>
                                    <select name="delivery_system_id" class="form-control">
                                        @foreach($delivery_systems as $delivery_system)
                                            <option value="{{ $delivery_system->id }}" {{ ($delivery_system->id == old('delivery_system_id')) ? 'selected' : '' }}>{{ $delivery_system->system_details }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('delivery_system_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('delivery_system_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Delivery Payment System</label>
                                    <select name="delivery_payment_system_id" onchange="getCheckBox()" id="payment_system" class="form-control">
                                        @foreach($delivery_payment_systems as $delivery_payment_system)
                                            <option value="{{ $delivery_payment_system->id }}" {{ ($delivery_payment_system->id == old('delivery_payment_system_id')) ? 'selected' : '' }}>{{ $delivery_payment_system->system_title }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('delivery_payment_system_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('delivery_payment_system_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div id="confirm_check_box" class="d-none">
                                    <div class="mb-20">
                                        <input type="checkbox" name="confirm_delivery_fee" id="basic_checkbox_2" class="filled-in">
                                        <label for="basic_checkbox_2">Have you got delivery fee?</label>
                                    </div>
                                </div>

                                <input type="submit" class="btn-success btn" value="Add Product">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section("javascript")
    <script>
        function getCheckBox() {
            var inputed_value = document.getElementById('payment_system').value;
            if (inputed_value == 1){
                document.getElementById('confirm_check_box').classList.remove('d-block');
                document.getElementById('basic_checkbox_2').removeAttribute('checked', '');
                document.getElementById('confirm_check_box').classList.add('d-none');
            }else{
                document.getElementById('confirm_check_box').classList.remove('d-none');
                document.getElementById('basic_checkbox_2').setAttribute('checked', '');
                document.getElementById('confirm_check_box').classList.add('d-block');
            }
        }

        function getDistrict() {
            var division_id = document.getElementById('division').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/district',
                data: {division_id:division_id},
                success: function (data) {
                    document.getElementById('district').innerHTML = data;
                }
            });
        }

        function getUpazila() {
            var district_id = document.getElementById('district').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/upazila',
                data: {district_id:district_id},
                success: function (data) {
                    document.getElementById('upazila').innerHTML = data;
                }
            });
        }
    </script>
@endsection
