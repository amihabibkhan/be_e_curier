@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Pending for approval List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($users) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>E-mail</th>
                                    <th>Profile Picture</th>
                                    <th>Branch</th>
                                    <th>User Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td class="text-center">
                                            <img src="{{ asset('storage') }}/{{ $user->profile_pic }}" height="50" alt="">
                                        </td>
                                        <td>{{ $user->branch->branch_name }}</td>
                                        <td>{{ $user->role->role }}</td>
                                        <td style="width: 10%;white-space: nowrap">
                                            <a href="{{ route('singleUserView', $user->id) }}" class="btn bg-black">Details</a>

                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#approve{{ $user->id }}">Approve</button>

                                            <div class="modal fade" id="approve{{ $user->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                    <div class="modal-content modal-col-red">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <h4>Are you sure to Approve?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="{{ route('userApproveConfirmation', $user->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#smallModal{{ $user->id }}">Delete</button>

                                            <div class="modal fade" id="smallModal{{ $user->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                    <div class="modal-content modal-col-red">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <h4>Are you sure to delete?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="{{ route('userDeleteConfirmation', $user->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>


@endsection