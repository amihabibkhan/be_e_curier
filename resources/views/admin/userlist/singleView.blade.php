@extends('layouts.adminlayout')



@section("main_content")
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        @if($user_info->profile_pic)
                            <img style="width: 60%" src="{{ asset('storage') }}/{{ $user_info->profile_pic }}" alt="" />
                        @else
                            <img style="width: 60%" src="{{ asset('admin') }}/images/user-lg.jpg" alt="" />
                        @endif
                    </div>
                    <div class="content-area">
                        <h3>{{ $user_info->name }}</h3>
                        <p>{{ $user_info->designation }}</p>
                        <p>{{ $user_info->role->role }}</p>
                        @if($user_info->role_id == 4)
                            <p>{{ ($user_info->working_status == 1) ? "Not Engaged" : "Working" }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="card">
                <div class="header">
                    <h2>User Details</h2>
                </div>
                <div class="body">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{ $user_info->name }}</td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td>{{ $user_info->designation }}</td>
                        </tr>
                        <tr>
                            <th>User Role</th>
                            <td>{{ $user_info->role->role }}</td>
                        </tr>
                        <tr>
                            <th>Branch</th>
                            <td>{{ $user_info->branch->branch_name }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{ $user_info->phone }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td><span style="background-color: gray; color: #fff; padding: 5px 10px">{{ $user_info->approve_status->status_title }}</span></td>
                        </tr>
                        @if($user_info->role_id == 3)
                        <tr>
                            <th>Company Name</th>
                            <td>{{ $user_info->company_name}}</td>
                        </tr>
                        <tr>
                            <th>Company fb Id</th>
                            <td>{{ $user_info->fb_fan_page}}</td>
                        </tr>
                        <tr>
                            <th>Company Website</th>
                            <td>{{ $user_info->website }}</td>
                        </tr>
                        <tr>
                            <th>Company Logo</th>
                            <td><img src="{{ asset('storage') }}/{{ $user_info->company_logo }}" height="100" alt=""></td>
                        </tr>
                        @endif
                        <tr>
                            <th>Division</th>
                            <td>{{ ($user_info->division_id) ? $user_info->division->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>District</th>
                            <td>{{ ($user_info->district_id) ? $user_info->district->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>Upazila</th>
                            <td>{{ ($user_info->upazila_id) ? $user_info->upazila->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>Area</th>
                            <td>{{ ($user_info->area_id) ? $user_info->area->area_name : "" }}</td>
                        </tr>
                        @if($user_info->role_id == 4)
                        <tr>
                            <th>NID Image</th>
                            <td><a href="{{ url('/storage')}}/{{ $user_info->nid_image }}" target="_blank">View Image</a></td>
                        </tr>
                        <tr>
                            <th>NID Number</th>
                            <td>{{ $user_info->nid_number }}</td>
                        </tr>
                        <tr>
                            <th>Permanent Address</th>
                            <td>{{ $user_info->permanent_address }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Present Address</th>
                            <td>{{ $user_info->present_address }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
