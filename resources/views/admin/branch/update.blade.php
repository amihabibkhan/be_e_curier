@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-12 ">
            <div class="card">
                <div class="header">
                    <h2>Updaate Branch Info</h2>
                </div>
                <form action="{{ route('branch.update', $branch->id) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="container-fluid">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">Branch Name</label>
                                    <input type="text" value="{{ $branch->branch_name }}" class="form-control" name="branch_name" placeholder="write a branch name">
                                    @if($errors->has('branch_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong style="color: red; font-style: italic">{{ $errors->first('branch_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Branch Manager</label>
                                    <select name="user_id"  class="form-control">
                                        <option disabled selected>-- Select a Manager--</option>
                                        @foreach($managers as $manager)
                                            <option value="{{ $manager->id }}" {{ ($manager->id == $branch->user_id) ? 'selected' : '' }}>{{ $manager->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Branch Phone</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Phone Number" value="{{ $branch->phone }}">
                                </div>
                                <div class="mb-20">
                                    <label for="">Division</label>
                                    <select name="division_id"  id="division" onchange="getDistrict()"  class="form-control">
                                        <option disabled selected>-- Select a Division --</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id }}" {{ ($division->id == $branch->division_id) ? 'selected' : '' }}>{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">District</label>
                                    <select name="district_id" onchange="getUpazila()" class="form-control" id="district">
                                        @foreach($districts as $district)
                                            <option value="{{ $district->id }}" {{ ($district->id == $branch->district_id) ? 'selected' : '' }}>{{ $district->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Upazila</label>
                                    <select name="upazila_id" class="form-control" id="upazila">
                                        @foreach($upazilas as $upazila)
                                            <option value="{{ $upazila->id }}" {{ ($upazila->id == $branch->upazila_id) ? 'selected' : '' }}>{{ $upazila->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control" value="{{ $branch->street }}" name="street_address" placeholder="Street Address">
                                </div>
                                <input type="submit" class="btn btn-success" value="Update Branch">
                            </div>
                        </div>
                    </div>
                    </form>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

    <script>
        function getDistrict() {
            var division_id = document.getElementById('division').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/district',
                data: {division_id:division_id},
                success: function (data) {
                    document.getElementById('district').innerHTML = data;
                }
            });
        }

        function getUpazila() {
            var district_id = document.getElementById('district').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/upazila',
                data: {district_id:district_id},
                success: function (data) {
                    document.getElementById('upazila').innerHTML = data;
                }
            });
        }
    </script>
@endsection
