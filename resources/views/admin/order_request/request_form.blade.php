@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Create a new order Request</h2>
                </div>
                <div class="body">
                    <form action="{{ route('order_request.store') }}" method="post">
                        @csrf
                        @merchant
                        <input type="hidden" name="merchant_id" value="{{ auth::id() }}">
                        @endmerchant
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                @manager
                                <div class="mb-20">
                                    <label for="">Select a Merchant</label>
                                    <select name="merchant_id" class="form-control">
                                        <option disabled selected>-- Select a Merchant --</option>
                                        @foreach($merchants as $merchant)
                                            <option value="{{ $merchant->id }}">{{ $merchant->name }} ({{ $merchant->phone }})</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endmanager
                                <div class="mb-20">
                                    <label for="">Product Quantity</label>
                                    <input type="number" name="product_quantity" class="form-control" placeholder="How many Product have to deliver">
                                    @if ($errors->has('product_quantity'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('product_quantity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Approximate Weight</label>
                                    <input type="number" name="weight" class="form-control" placeholder="Approximate weight of products">
                                    @if ($errors->has('weight'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('weight') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <input type="submit" class="btn btn-success" value="Create Request">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection