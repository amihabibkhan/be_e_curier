@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2 style="margin-bottom: 15px">
                        Delivery Fee History
                    </h2>
                    <h2>(Total Paid - {{ $total_payment }}/- TK)</h2>

                    @if(auth::user()->role_id == 1 || auth::user()->role_id == 2 )

                    <button type="button" class="header-dropdown m-r--5 btn btn-success" data-toggle="modal" data-target="#make_payment">Get Payment</button>

                    <div style="white-space: normal" class="modal fade" id="make_payment" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-center" role="document">
                            <div class="modal-content ">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="smallModalLabel">Get Payment</h4>
                                </div>
                                <form action="{{ route('getDeliveryFeeFromMerchant') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="mb-20">
                                            <label for="">Select a Merchant</label>
                                            <select name="merchant_id" id="sel" onchange="get_id(this)" class="form-control">
                                                <option value="">-- Select a Merchant --</option>
                                                @foreach($merchants as $merchant)
                                                    <option id="{{ $merchant->merchantTotalDeliveryFee() - $merchant->merchantDeliveryFeePaid() }}" value="{{ $merchant->id }}">{{ $merchant->name }} ({{ $merchant->phone }})</option>
                                                @endforeach
                                            </select>

                                            <script>
                                                function get_id (s){
                                                    var sel = document.getElementById('sel');
                                                    var adID = sel[sel.selectedIndex].id;
                                                    document.getElementById('output_of_amount').innerHTML = adID;
                                                    document.getElementById('due').value = adID;
                                                }
                                            </script>
                                            <input type="hidden" name="due" id="due" value="">
                                        </div>
                                        <div class="mb-20">
                                            <label for="" style="font-size: 25px">Due : <span style="margin-left: 15px; color: #000;" id="output_of_amount">0</span>/- TK</label>
                                        </div>
                                        <div class="mb-20">
                                            <label for="">Payment Amount</label>
                                            <input type="text" name="amount" class="form-control" placeholder="Set a payment amount">
                                        </div>
                                        <div class="mb-20">
                                            <label for="">Note</label>
                                            <textarea name="note" class="form-control" placeholder="Write a note related to this payment"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Make Payment</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    @endif
                    {{-- new payment end --}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <style>
                            .pending{
                                padding: 5px 10px;
                                background-color: #9f105c;
                                color: #fff;
                            }
                            .success{
                                padding: 5px 10px;
                                background-color: #0d95e8;
                                color: #fff;
                            }
                            .rejected{
                                padding: 5px 10px;
                                background-color: red;
                                color: #fff;
                            }
                        </style>
                        @if(count($payments) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Paid By</th>
                                    <th>Paid To</th>
                                    <th class="text-center">Amount</th>
                                    <th>Payment Status</th>
                                    <th>Note</th>
                                    <th>Paid At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->get_paid_to->name }}</td>
                                        <td>{{ $payment->get_merchant_info->name }}</td>
                                        <td class="text-center">{{ $payment->amount }} /-</td>
                                        <td>
                                            @if($payment->status == 0)
                                                <span class='pending'>Pending</span>
                                            @elseif($payment->status == 1)
                                                <span class='success'>Success</span>
                                            @else
                                                <span class='rejected'>Rejected</span>
                                            @endif
                                        </td>
                                        <td title="{{ $payment->note }}">{{ str_limit($payment->note, 30) }}</td>
                                        <td class="text-center">{{ $payment->created_at->diffForHumans() }}</td>
                                        <td>
                                            @if(auth::user()->role_id == 3)
                                                @if($payment->status == 0)
                                                    <button type="button" class="btn bg-green" data-toggle="modal" data-target="#accept{{ $payment->id }}">Accept</button>

                                                    <div style="white-space: normal" class="modal fade" id="accept{{ $payment->id }}" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                            <div class="modal-content modal-col-red">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="smallModalLabel">Accept Confirmation!</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure to accept?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a href="{{ route('acceptOrRejectMerchantDeliveryFee', ['accept', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn bg-red" data-toggle="modal" data-target="#reject{{ $payment->id }}">Reject</button>

                                                    <div style="white-space: normal" class="modal fade" id="reject{{ $payment->id }}" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                            <div class="modal-content modal-col-red">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="smallModalLabel">Reject Confirmation!</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure to reject?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a href="{{ route('acceptOrRejectMerchantDeliveryFee', ['reject', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            {{-- action button for admin start --}}
                                            @if(auth::user()->role_id == 1 || auth::user()->role_id == 2 )
                                            @if($payment->status == 2)
                                                <button type="button" class="btn bg-green" data-toggle="modal" data-target="#re_payment{{ $payment->id }}">Re Payment</button>

                                                <div style="white-space: normal" class="modal fade" id="re_payment{{ $payment->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content modal-col-red">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Re-Payment Request</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure to Re-Payment?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('changeMerchantFeeStatus', ['re-payment', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($payment->status != 1)
                                                <button type="button" class="btn bg-red" data-toggle="modal" data-target="#delete{{ $payment->id }}">Delete</button>

                                                <div style="white-space: normal" class="modal fade" id="delete{{ $payment->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content modal-col-red">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Delete Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure to Delete?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('changeMerchantFeeStatus', ['delete', $payment->id]) }}" type="submit" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @endif
                                            {{-- action button for admin end --}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection