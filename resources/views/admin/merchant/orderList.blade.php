@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>
                        List of {{ ucfirst($type) }} Orders
                    </h2>
                </div>
                <style>
                    ._1{
                        padding: 5px 10px; background-color: #1b4b72; color: #fff
                    }
                    ._2{
                        padding: 5px 10px; background-color: #9f105c; color: #fff
                    }
                    ._3{
                        padding: 5px 10px; background-color: #4a148c; color: #fff
                    }
                    ._4{
                        padding: 5px 10px; background-color: #0d47a1; color: #fff
                    }
                    ._5{
                        padding: 5px 10px; background-color: #5b32bc; color: #fff;
                    }
                    ._6{
                        padding: 5px 10px; background-color: #7ecff4; color: #000;
                    }
                    ._7{
                        padding: 5px 10px; background-color: #811b1b; color: #fff;
                    }
                    ._8{
                        padding: 5px 10px; background-color: #000; color: #fff;
                    }
                    ._9{
                        padding: 5px 10px; background-color: red; color: #fff;
                    }
                    ._10{
                        padding: 5px 10px; background-color: #4C81C9; color: #fff;
                    }
                    ._11{
                        padding: 5px 10px; background-color: #2d995b; color: #fff;
                    }
                    ._12{
                        padding: 5px 10px; background-color: #9C27B0; color: #fff;
                    }
                    ._13{
                        padding: 5px 10px; background-color: #935c25; color: #fff;
                    }
                </style>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($products) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Order ID</th>
                                    <th>Receiving Branch</th>
                                    <th>Receiver</th>
                                    <th>Receiver Phone</th>
                                    <th class="text-center">Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <?php
                                    switch ($product->order_status_id){
                                        case 1: $class = "_1";
                                            break;
                                        case 2: $class = "_2";
                                            break;
                                        case 3: $class = "_3";
                                            break;
                                        case 4: $class = "_4";
                                            break;
                                        case 5: $class = "_5";
                                            break;
                                        case 6: $class = "_6";
                                            break;
                                        case 7: $class = "_7";
                                            break;
                                        case 8: $class = "_8";
                                            break;
                                        case 9: $class = "_9";
                                            break;
                                        case 10: $class = "_10";
                                            break;
                                        case 11: $class = "_11";
                                            break;
                                        case 12: $class = "_12";
                                            break;
                                        case 13: $class = "_13";
                                            break;
                                    }
                                    ?>
                                    <tr>
                                        <td>{{ $product->tracking_id }}</td>
                                        <td>{{ $product->order->tracking_id }}</td>
                                        <td>{{ $product->toBranch->branch_name }}</td>
                                        <td>{{ $product->receiver_name }}</td>
                                        <td>{{ $product->receiver_phone }}</td>
                                        <td class="text-center" style="white-space: nowrap">
                                            <span class="{{ $class }}">{{ $product->order_status->status_title }}</span>
                                            @if($product->order_status_id == 9)
                                            <br>
                                            <p style="padding-top: 15px">{{ $product->cancel_reason->reason_title }}</p>
                                            @endif
                                        </td>
                                        <td class="text-center">{{ $product->created_at->diffForHumans() }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            {{-- view start --}}
                                            <button type="button" class="btn bg-green" data-toggle="modal" data-target="#view{{ $product->id }}">View</button>
                                            <div style="white-space: normal" class="modal fade" id="view{{ $product->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Product Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-bordered table-striped table-hover">
                                                                <tr>
                                                                    <th>Product ID</th>
                                                                    <td>{{ $product->tracking_id }}</td>
                                                                    <th>Order ID</th>
                                                                    <td>{{ $product->order->tracking_id }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>From Branch</th>
                                                                    <td>{{ $product->fromBranch->branch_name }}</td>
                                                                    <th>To Branch</th>
                                                                    <td>{{ $product->toBranch->branch_name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Product Description</th>
                                                                    <td colspan="3">{{ $product->product_description }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Receiver Name</th>
                                                                    <td>{{ $product->receiver_name }}</td>
                                                                    <th>Receiver Phone</th>
                                                                    <td>{{ $product->receiver_phone }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Address</th>
                                                                    <td colspan="3">{{ $product->area->area_name }}, {{ $product->upazila->name }}, {{ $product->district->name }}, {{ $product->division->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Order Status</th>
                                                                    <td>{{ $product->order_status->status_title }}</td>
                                                                    <th>Total Amount</th>
                                                                    <td>{{ $product->total_amount }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Delivery System</th>
                                                                    <td>{{ $product->delivery_system->system_details }}</td>
                                                                    <th>Delivery Fee Status</th>
                                                                    <td>{{ ($product->delivery_fee_payment_status == 0) ? "Unpaid" : "Paid"  }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Delivery Payment System</th>
                                                                    <td colspan="3">{{ $product->delivery_payment_system->system_title }}</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- view end --}}


                                            {{-- complete confirm start --}}
                                            @if($product->order_status_id == 12)
                                                <button type="button" class="btn bg-brown" data-toggle="modal" data-target="#confirm_complete{{ $product->id }}">Receive</button>
                                                <div style="white-space: normal" class="modal fade" id="confirm_complete{{ $product->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                                        <div class="modal-content text-center modal-col-brown">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>Have you got the Product back?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="{{ route('changeStatusByMerchant', [$product->id, 13]) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.2)">Yes</a>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{-- complete confirm end --}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection