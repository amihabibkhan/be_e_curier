@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>All Notification List</h2>
                </div>
                <div class="body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Title</th>
                            <th>Details</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        @foreach($allNotifications as $notification)
                            <tr>
                                <td>{{ $notification->type }}</td>
                                <td>{{ $notification->details }}</td>
                                <td>{{ $notification->created_at->diffForHumans() }}</td>
                                <td>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#smallModal{{ $notification->id }}">Delete</button>

                                    <div class="modal fade" id="smallModal{{ $notification->id }}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-dialog-center modal-sm" role="document">
                                            <div class="modal-content modal-col-red">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="smallModalLabel">Confirmation!</h4>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <h4>Are you sure to Delete?</h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{ route('notificationDelete', $notification->id) }}" class="btn btn-link" style="background-color: rgba(0,0,0,.1)">Yes</a>
                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $notifications->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
