@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Delivery System List and Price
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($systems) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Delivery System</th>
                                    <th>Picker Boy Fee</th>
                                    <th>Delivery Boy Fee</th>
                                    <th>Company Profit</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($systems as $system)
                                    <tr>
                                        <td>{{ $system->system_details }}</td>
                                        <td>{{ $system->receiver_boy_fee }}</td>
                                        <td>{{ $system->delivery_boy_fee }}</td>
                                        <td>{{ $system->company_fee }}</td>
                                        <td>{{ $system->company_fee + $system->delivery_boy_fee + $system->receiver_boy_fee }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            <button type="button" class="btn bg-black" data-toggle="modal" data-target="#smallModal{{ $system->id }}">Update</button>

                                            <div class="modal fade" style="white-space: normal;" id="smallModal{{ $system->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content modal-col-green">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Update Delivery System Info</h4>
                                                        </div>
                                                        <form action="{{ route('delivery_system.update', $system->id) }}" method="post">
                                                            @csrf
                                                            @method("put")
                                                            <div class="modal-body">
                                                                <div class="mb-20">
                                                                    <label for="">Delivery Details</label>
                                                                    <input type="text" style="width: 100%" value="{{ $system->system_details }}" class="form-control" name="system_details" placeholder="ex: In Dhaka One Day - 60 TK">
                                                                </div>
                                                                <div class="mb-20">
                                                                    <label for="">Picker Boy Fee</label>
                                                                    <input type="text" style="width: 100%" value="{{ $system->receiver_boy_fee }}" class="form-control" name="receiver_boy_fee" placeholder="Picker or Receiver Boy Fee">
                                                                </div>
                                                                <div class="mb-20">
                                                                    <label for="">Delivery Boy Fee</label>
                                                                    <input type="text" style="width: 100%" value="{{ $system->delivery_boy_fee }}" class="form-control" name="delivery_boy_fee" placeholder="Delivery Boy Fee">
                                                                </div>
                                                                <div class="mb-20">
                                                                    <label for="">Company Profit</label>
                                                                    <input type="text" style="width: 100%" value="{{ $system->company_fee }}" class="form-control" name="company_fee" placeholder="Company Profit">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-link waves-effect" >Update</button>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="header">
                    <h2>Create a New Area</h2>
                </div>
                <div class="body">
                    <form action="{{ route('delivery_system.store') }}" method="post">
                        @csrf
                        <div class="mb-20">
                            <label for="">Delivery Details</label>
                            <input type="text" class="form-control" name="system_details" placeholder="ex: In Dhaka One Day - 60 TK">
                            @if($errors->has('system_details'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: red; font-style: italic">{{ $errors->first('system_details') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="mb-20">
                            <label for="">Picker Boy Fee</label>
                            <input type="text" class="form-control" name="receiver_boy_fee" placeholder="Picker or Receiver Boy Fee">
                            @if($errors->has('receiver_boy_fee'))
                                <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('receiver_boy_fee') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="mb-20">
                            <label for="">Delivery Boy Fee</label>
                            <input type="text" class="form-control" name="delivery_boy_fee" placeholder="Delivery Boy Fee">
                            @if($errors->has('delivery_boy_fee'))
                                <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('delivery_boy_fee') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="mb-20">
                            <label for="">Company Profit</label>
                            <input type="text" class="form-control" name="company_fee" placeholder="Company Profit">
                            @if($errors->has('company_fee'))
                                <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('company_fee') }}</strong>
                            </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-success" value="Create Delivery System">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>


@endsection