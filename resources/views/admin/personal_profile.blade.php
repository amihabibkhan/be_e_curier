@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-3">
            <div class="card profile-card">
                <div class="profile-header">&nbsp;</div>
                <div class="profile-body">
                    <div class="image-area">
                        @if(auth::user()->profile_pic)
                            <img style="width: 60%" src="{{ asset('storage') }}/{{ auth::user()->profile_pic }}" alt="" />
                        @else
                            <img style="width: 60%" src="{{ asset('admin') }}/images/user-lg.jpg" alt="" />
                        @endif
                    </div>
                    <div class="content-area">
                        <h3>{{ auth::user()->name }}</h3>
                        <p>{{ auth::user()->designation }}</p>
                        <p>{{ auth::user()->role->role }}</p>
                        @boy
                        <p>{{ (auth::user()->working_status == 1) ? "Not Engaged" : "Engaged" }}</p>
                        @endboy
                    </div>
                </div>
                <div class="profile-footer">
                    <ul>
                        @managerOnly
                        {{-- delivery fee --}}
                        <li>
                            <span>Total Delivery Fee</span>
                            <span>{{ $manger_total_delivery_fee }} TK</span>
                        </li>
                        <li>
                            <span>Payment Complete</span>
                            <span>{{ $manger_completed_delivery_fee }} TK</span>
                        </li>
                        <li>
                            <span>Unpaid Amount</span>
                            <span>{{ $manger_total_delivery_fee - $manger_completed_delivery_fee }} TK</span>
                        </li>
                        {{-- product price --}}
                        <li style="border-top: 1px solid black">
                            <span>Total Product Price</span>
                            <span>{{ $manger_total_product_price }} TK</span>
                        </li>
                        <li>
                            <span>Paid to merchant</span>
                            <span>{{ $manger_paid_to_merchant }} TK</span>
                        </li>
                        <li>
                            <span>Unpaid Amount</span>
                            <span>{{ $manger_total_product_price - $manger_paid_to_merchant }} TK</span>
                        </li>

                        {{-- pick orders --}}
                        <li style="border-top: 1px solid black">
                            <span>Total Pick Order</span>
                            <span>{{ $manger_total_order }}</span>
                        </li>
                        <li>
                            <span>Completed Pick Order</span>
                            <span>{{ $manger_delivery_completed }}</span>
                        </li>
                        <li>
                            <span>Processing Pick Order</span>
                            <span>{{ $manger_processing_order }}</span>
                        </li>
                        <li>
                            <span>Canceled Pick Order</span>
                            <span>{{ $manger_canceled_order }}</span>
                        </li>
                        {{-- delivery orders --}}
                        <li style="border-top: 1px solid black">
                            <span>Total Delivery Order</span>
                            <span>{{ $manger_total_order_delivery }}</span>
                        </li>
                        <li>
                            <span>Completed Delivery Order</span>
                            <span>{{ $manger_delivery_completed_delivery }}</span>
                        </li>
                        <li>
                            <span>Processing Delivery Order</span>
                            <span>{{ $manger_processing_order_delivery }}</span>
                        </li>
                        <li>
                            <span>Canceled Delivery Order</span>
                            <span>{{ $manger_canceled_order_delivery }}</span>
                        </li>
                        {{-- merchant list --}}
                        <li style="border-top: 1px solid black">
                            <span>Active Merchant</span>
                            <span>{{ $manager_total_merchant_active }}</span>
                        </li>
                        <li>
                            <span>Pending Merchant</span>
                            <span>{{ $manager_total_merchant_pending }}</span>
                        </li>
                        <li>
                            <span>Suspended Merchant</span>
                            <span>{{ $manager_total_merchant_suspended }}</span>
                        </li>
                        {{-- delivery man --}}
                        <li style="border-top: 1px solid black">
                            <span>Active Delivery Man</span>
                            <span>{{ $manager_total_delivery_active }}</span>
                        </li>
                        <li>
                            <span>Pending Delivery Man</span>
                            <span>{{ $manager_total_delivery_pending }}</span>
                        </li>
                        <li>
                            <span>Suspended Delivery Man</span>
                            <span>{{ $manager_total_delivery_suspended }}</span>
                        </li>
                        @endmanagerOnly
                        @merchant
                        <li>
                            <span>Total Product Price</span>
                            <span>{{ $merchant_total_product_price }} TK</span>
                        </li>
                        <li>
                            <span>Total Received</span>
                            <span>{{ $merchant_received }} TK</span>
                        </li>
                        <li>
                            <span>Available Balance</span>
                            <span>{{ $merchant_total_product_price - $merchant_received }} TK</span>
                        </li>
                        <li style="border-top: 1px solid black">
                            <span>Total Delivery Fee</span>
                            <span>{{ $merchant_delivery_fee }} TK</span>
                        </li>
                        <li>
                            <span>Delivery Fee Paid</span>
                            <span>{{ $merchant_delivery_fee_paid }} TK</span>
                        </li>
                        <li>
                            <span>Delivery Fee Due</span>
                            <span>{{ $merchant_delivery_fee - $merchant_delivery_fee_paid }} TK</span>
                        </li>
                        <li style="border-top: 1px solid black">
                            <span>Total Order</span>
                            <span>{{ $merchant_total_order }}</span>
                        </li>
                        <li>
                            <span>Completed Order</span>
                            <span>{{ $merchant_delivery_completed }}</span>
                        </li>
                        <li>
                            <span>Processing Order</span>
                            <span>{{ $merchant_processing_order }}</span>
                        </li>
                        <li>
                            <span>Canceled Order</span>
                            <span>{{ $merchant_canceled_order }}</span>
                        </li>
                        @endmerchant
                        @boy
                        <li>
                            <span>Available Balance</span>
                            <span>{{ $boy_balance }} TK</span>
                        </li>
                        <li>
                            <span>Total Paid</span>
                            <span>{{ $boy_paid }} TK</span>
                        </li>
                        <li>
                            <span>Total Income</span>
                            <span>{{ $boy_total_income }} TK</span>
                        </li>
                        <li>
                            <span>Delivery Pending</span>
                            <span>{{ $boy_delivery_pending }}</span>
                        </li>
                        <li>
                            <span>Pick order Pending</span>
                            <span>{{ $boy_picking_pending }}</span>
                        </li>
                        <li>
                            <span>Completed Order</span>
                            <span>{{ $boy_order_completed }}</span>
                        </li>
                        <li>
                            <span>Rejected Order</span>
                            <span>{{ $boy_order_rejected }}</span>
                        </li>
                        @endboy
                    </ul>
{{--                    <button class="btn btn-primary btn-lg waves-effect btn-block">FOLLOW</button>--}}
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="card">
                <div class="header">
                    <h2>User Details</h2>
                </div>
                <div class="body">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{ auth::user()->name }}</td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td>{{ auth::user()->designation }}</td>
                        </tr>
                        <tr>
                            <th>User Role</th>
                            <td>{{ auth::user()->role->role }}</td>
                        </tr>
                        <tr>
                            <th>Branch</th>
                            <td>{{ auth::user()->branch->branch_name }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{ auth::user()->phone }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td><span style="background-color: gray; color: #fff; padding: 5px 10px">{{ auth::user()->approve_status->status_title }}</span></td>
                        </tr>
                        @exceptboy
                        <tr>
                            <th>Company Name</th>
                            <td>{{ auth::user()->company_name}}</td>
                        </tr>
                        <tr>
                            <th>Company fb Id</th>
                            <td>{{ auth::user()->fb_fan_page}}</td>
                        </tr>
                        <tr>
                            <th>Company Website</th>
                            <td>{{ auth::user()->website }}</td>
                        </tr>
                        <tr>
                            <th>Company Logo</th>
                            <td><img src="{{ asset('storage') }}/{{ auth::user()->company_logo }}" height="100" alt=""></td>
                        </tr>
                        @endexceptboy
                        <tr>
                            <th>Division</th>
                            <td>{{ (auth::user()->division_id) ? auth::user()->division->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>District</th>
                            <td>{{ (auth::user()->district_id) ? auth::user()->district->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>Upazila</th>
                            <td>{{ (auth::user()->upazila_id) ? auth::user()->upazila->name : "" }}</td>
                        </tr>
                        <tr>
                            <th>Area</th>
                            <td>{{ (auth::user()->area_id) ? auth::user()->area->area_name : "" }}</td>
                        </tr>
                        @except_merchant
                        <tr>
                            <th>NID Image</th>
                            <td><a href="{{ url('/storage')}}/{{ auth::user()->nid_image }}" target="_blank">View Image</a></td>
                        </tr>
                        <tr>
                            <th>NID Number</th>
                            <td>{{ auth::user()->nid_number }}</td>
                        </tr>
                        <tr>
                            <th>Permanent Address</th>
                            <td>{{ auth::user()->permanent_address }}</td>
                        </tr>
                        @endexcept_merchant
                        <tr>
                            <th>Present Address</th>
                            <td>{{ auth::user()->present_address }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
