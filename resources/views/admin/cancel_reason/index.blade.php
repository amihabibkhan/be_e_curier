@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Delivery Cancel Reasons
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($reasons) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Cancel Reason Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reasons as $reason)
                                    <tr>
                                        <td>{{ $reason->reason_title }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            <button type="button" class="btn bg-black" data-toggle="modal" data-target="#smallModal{{ $reason->id }}">Update</button>

                                            <div class="modal fade" style="white-space: normal;" id="smallModal{{ $reason->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content modal-col-green">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Update Delivery System Info</h4>
                                                        </div>
                                                        <form action="{{ route('cancel_reason.update', $reason->id) }}" method="post">
                                                            @csrf
                                                            @method("put")
                                                            <div class="modal-body">
                                                                <div class="mb-20">
                                                                    <label for="">Cancel Reason Title</label>
                                                                    <input type="text" value="{{ $reason->reason_title }}" style="width: 100%" class="form-control" name="reason_title" placeholder="Cancel reason title">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-link waves-effect" >Update</button>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="header">
                    <h2>Create a New Reason</h2>
                </div>
                <div class="body">
                    <form action="{{ route('cancel_reason.store') }}" method="post">
                        @csrf
                        <div class="mb-20">
                            <label for="">Cancel Reason Title</label>
                            <input type="text" class="form-control" name="reason_title" placeholder="Cancel reason title">
                            @if($errors->has('reason_title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: red; font-style: italic">{{ $errors->first('reason_title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-success" value="Add Cancel Reason">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>


@endsection