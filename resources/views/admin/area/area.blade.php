@extends('layouts.adminlayout')

@section("css")
    <link href="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection


@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        List of Area
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        @if(count($areas) > 0)
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Name of Area</th>
                                    <th>Branch Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($areas as $area)
                                    <tr>
                                        <td>{{ $area->area_name }}</td>
                                        <td>{{ ($area->branch_id) ? $area->branch->branch_name : "" }}</td>
                                        <td style="width: 10%; white-space: nowrap">
                                            <button type="button" class="btn bg-black" data-toggle="modal" data-target="#smallModal{{ $area->id }}">Update</button>

                                            <div class="modal fade" style="white-space: normal;" id="smallModal{{ $area->id }}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-center" role="document">
                                                    <div class="modal-content modal-col-green">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="smallModalLabel">Update Area Info</h4>
                                                        </div>
                                                        <form action="{{ route('area.update', $area->id) }}" method="post">
                                                            @csrf
                                                            @method("put")
                                                            <div class="modal-body">
                                                                <div class="mb-20">
                                                                    <label for="">Area Name</label>
                                                                    <input type="text" style="width: 100%" value="{{ $area->area_name }}" class="form-control" name="area_name" placeholder="write an area name">
                                                                </div>
                                                                <div class="mb-20">
                                                                    <label for="">Select a Branch</label>
                                                                    <select name="branch_id" style="width: 100%" class="form-control">
                                                                        @foreach($branches as $branch)
                                                                            <option value="{{ $branch->id }}" {{ ($branch->id == $area->branch_id) ? "selected" : "" }}>{{ $branch->branch_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-link waves-effect" >Update</button>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2 class="text-center mb-30">No Data Found</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="header">
                    <h2>Create a New Area</h2>
                </div>
                <div class="body">
                    <form action="{{ route('area.store') }}" method="post">
                        @csrf
                        <div class="mb-20">
                            <label for="">Area Name</label>
                            <input type="text" class="form-control" name="area_name" placeholder="write an area name">
                            @if($errors->has('area_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: red; font-style: italic">{{ $errors->first('area_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="mb-20">
                            <label for="">Select a Branch</label>
                            <select name="branch_id" class="form-control">
                                <option disabled selected>-- Please Select --</option>
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('branch_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: red; font-style: italic">{{ $errors->first('branch_id') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-success" value="Create Area">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    {{-- --}}
    <script src="{{ asset('admin') }}/js/pages/tables/jquery-datatable.js"></script>


@endsection