@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        {{-- delivery start --}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">layers</i>
                </div>
                <div class="content">
                    <div class="text">Total Branch</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_branch }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">Total Delivery Fee</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_delivery_fee }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">done</i>
                </div>
                <div class="content">
                    <div class="text">Payment Completed</div>
                    <div class="number count-to" data-from="0" data-to="{{ $completed_delivery_fee }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">do_not_disturb</i>
                </div>
                <div class="content">
                    <div class="text">Unpaid Delivery Fee</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_delivery_fee - $completed_delivery_fee }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        {{-- payment of merchant --}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">receipt</i>
                </div>
                <div class="content">
                    <div class="text">Total Products</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_products }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">payment</i>
                </div>
                <div class="content">
                    <div class="text">Total Product Price</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_product_price }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">done_all</i>
                </div>
                <div class="content">
                    <div class="text">Paid to merchant</div>
                    <div class="number count-to" data-from="0" data-to="{{ $paid_to_merchant }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="content">
                    <div class="text">Unpaid Amount</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_product_price - $paid_to_merchant }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        {{-- pick order --}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">list</i>
                </div>
                <div class="content">
                    <div class="text">Total Order</div>
                    <div class="number count-to" data-from="0" data-to="{{ $total_order }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">input</i>
                </div>
                <div class="content">
                    <div class="text">Completed Order</div>
                    <div class="number count-to" data-from="0" data-to="{{ $delivery_completed }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">find_replace</i>
                </div>
                <div class="content">
                    <div class="text">Processing Order</div>
                    <div class="number count-to" data-from="0" data-to="{{ $delivery_processing }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">eject</i>
                </div>
                <div class="content">
                    <div class="text">Canceled Order</div>
                    <div class="number count-to" data-from="0" data-to="{{ $delivery_canceled }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        {{-- delivery man and merchant --}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">account_box</i>
                </div>
                <div class="content">
                    <div class="text">Active Merchant</div>
                    <div class="number count-to" data-from="0" data-to="{{ $active_merchant }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">account_circle</i>
                </div>
                <div class="content">
                    <div class="text">Suspended Merchant</div>
                    <div class="number count-to" data-from="0" data-to="{{ $suspend_merchant }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">assignment_ind</i>
                </div>
                <div class="content">
                    <div class="text">Pending Merchant</div>
                    <div class="number count-to" data-from="0" data-to="{{ $pending_merchant }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">accessible</i>
                </div>
                <div class="content">
                    <div class="text">Active Delivery Man</div>
                    <div class="number count-to" data-from="0" data-to="{{ $active_boy }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">bug_report</i>
                </div>
                <div class="content">
                    <div class="text">Pending Delivery Man</div>
                    <div class="number count-to" data-from="0" data-to="{{ $pending_boy }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">delete_forever</i>
                </div>
                <div class="content">
                    <div class="text">Suspended Delivery Man</div>
                    <div class="number count-to" data-from="0" data-to="{{ $suspend_boy }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">notifications_active</i>
                </div>
                <div class="content">
                    <div class="text">Notifications</div>
                    <div class="number count-to" data-from="0" data-to="{{ $notification }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">pages</i>
                </div>
                <div class="content">
                    <div class="text">Delivery System</div>
                    <div class="number count-to" data-from="0" data-to="{{ $delivery_system }}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>

    {{-- chart js --}}
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Order Statistics</h2>
                </div>
                <div class="body">
                    <canvas id="bar_chart" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    <!-- Chart Plugins Js -->
    <script src="{{ asset('admin') }}/plugins/chartjs/Chart.bundle.js"></script>

    <script>
        $(function () {
            new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
        });
        function getChartJs(type) {
            var config = null;

            if (type === 'bar') {
                config = {
                    type: 'bar',
                    data: {
                        labels: {!! $months !!},
                        datasets: [{
                            label: "Total Order ",
                            data: {!! $month_data !!},
                            backgroundColor: 'rgba(244, 67, 54, 0.8)'
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            return config;
        }
    </script>
@endsection