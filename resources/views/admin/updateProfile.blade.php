@extends('layouts.adminlayout')

@section("main_content")
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Update Your Profile </h2>
                </div>
                <div class="body">
                    <form method="POST" action="{{ route('userRegistration.update') }}" enctype="multipart/form-data">
                        @if(auth::user()->role_id == 3)
                            <input type="hidden" value="3" name="role_id">
                        @elseif(auth::user()->role_id == 4)
                            <input type="hidden" value="4" name="role_id">
                        @elseif(auth::user()->role_id == 2)
                            <input type="hidden" value="2" name="role_id">
                        @else
                            <input type="hidden" value="1" name="role_id">
                        @endif

                            <input type="hidden" name="id" value="{{ auth::user()->id }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <label for="">Name</label>
                                    <input type="text" value="{{ auth::user()->name }}" class="form-control" placeholder="Name" name="name">
                                    @if($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Profile Picture (200x200)</label><br>
                                    <img src="{{ asset('storage') }}/{{ auth::user()->profile_pic }}" height="50" style="margin: 10px 0 15px" alt="">
                                    <input type="file" class="form-control" name="profile_pic">
                                </div>
                                @if(auth::user()->role_id == 3)
                                    <div class="mb-20">
                                        <label for="">Post / Designation</label>
                                        <input type="text" value="{{ auth::user()->designation }}" class="form-control" placeholder="Post or Designation" name="designation">
                                    </div>
                                @endif
                                <div class="mb-20">
                                    <label for="">E-mail</label>
                                    <input type="email" class="form-control" value="{{ auth::user()->email }}" placeholder="E-mail Address" name="email">
                                    @if($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Old Password</label>
                                    <input type="password" class="form-control" placeholder="Provide your old password" name="old_password">
                                </div>
                                <div class="mb-20">
                                    <label for="">New Password</label>
                                    <input type="password" class="form-control" placeholder="Create a new strong password" name="password">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong style="color: red; font-style: italic">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Phone Number</label>
                                    <input type="text" class="form-control" value="{{ auth::user()->phone }}" placeholder="Phone Number" name="phone">
                                    @if($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- merchant start --}}
                                @if(auth::user()->role_id == 3)
                                    <div class="mb-20">
                                        <label for="">Company Name</label>
                                        <input type="text" class="form-control" value="{{ auth::user()->company_name }}" placeholder="Company Name" name="company_name">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">Company Facebook Fan Page</label>
                                        <input type="text" class="form-control" value="{{ auth::user()->fb_fan_page }}" placeholder="Facebook Fan Page Link" name="fb_fan_page">
                                    </div>
                                @endif
                                {{-- merchant end --}}

                                {{-- delivery boy start --}}
                                @if(auth::user()->role_id == 4)
                                    <div class="mb-20">
                                        <label for="">NID/Birth Certificate Scan Copy</label>
                                        <input type="file" class="form-control" name="nid_image">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">NID/Birth Certificate Number</label>
                                        <input type="text" class="form-control" value="{{ auth::user()->nid_number }}" placeholder="NID or Birth Certificate Number" name="nid_number">
                                    </div>
                                @endif
                                {{-- delivery boy end --}}
                            </div>
                            <div class="col-md-6">

                                {{-- merchant start --}}
                                @if(auth::user()->role_id == 3)
                                    <div class="mb-20">
                                        <label for="">Company Website</label>
                                        <input type="text" value="{{ auth::user()->website }}" class="form-control" placeholder="Company Website Link" name="website">
                                    </div>
                                    <div class="mb-20">
                                        <label for="">Company Logo</label><br>
                                        <img src="{{ asset('storage') }}/{{ auth::user()->company_logo }}" height="50" style="margin: 10px 0 15px" alt="">
                                        <input type="file" class="form-control" name="company_logo">
                                    </div>
                                @endif
                                {{-- merchant end --}}

                                <div class="mb-20">
                                    <label for="">Branch</label>
                                    <select name="branch_id" class="form-control">
                                        <option disabled selected>-- Select a Branch --</option>
                                        @foreach($branches as $branch)
                                            <option value="{{ $branch->id }}" {{ ($branch->id == auth::user()->branch_id) ? 'selected' : '' }}>{{ $branch->branch_name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('branch_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: red; font-style: italic">{{ $errors->first('branch_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-20">
                                    <label for="">Division</label>
                                    <select name="division_id"  id="division" onchange="getDistrict()"  class="form-control">
                                        <option disabled selected>-- Select a Division --</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id }}" {{ ($division->id == auth::user()->division_id) ? 'selected' : '' }}>{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">District</label>
                                    <select name="district_id" onchange="getUpazila()" class="form-control" id="district">
                                        <option disabled selected>Select a District</option>
                                        @foreach($districts as $district)
                                            <option value="{{ $district->id }}" {{ ($district->id == auth::user()->district_id) ? 'selected' : '' }}>{{ $district->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Upazila</label>
                                    <select name="upazila_id" class="form-control" id="upazila">
                                        <option disabled selected>Select an Upazila</option>
                                        @foreach($upazilas as $upazila)
                                            <option value="{{ $upazila->id }}" {{ ($upazila->id == auth::user()->upazila_id) ? 'selected' : '' }}>{{ $upazila->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Area</label>
                                    <select name="area_id" class="form-control">
                                        <option disabled selected>Select an Area </option>
                                        @foreach($areas as $area)
                                            <option value="{{ $area->id }}" {{ ($area->id == auth::user()->area_id) ? 'selected' : '' }}>{{ $area->area_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-20">
                                    <label for="">Present Address</label>
                                    <input type="text" class="form-control" value="{{ auth::user()->present_address }}" placeholder="Present Address" name="present_address">
                                </div>
                                {{-- delivery boy start --}}
                                @if(auth::user()->role_id == 4)
                                    <div class="mb-20">
                                        <label for="">Permanent Address</label>
                                        <input type="text" class="form-control" placeholder="Permanent Address" value="{{ auth::user()->parmanent_address }}" name="parmanent_address">
                                    </div>
                                @endif
                                {{-- delivery boy end --}}
                               <div class="text-right">
                                   <input type="submit" class="btn btn-success btn-lg" value="Update Info">
                               </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    <script>
        function getDistrict() {
            var division_id = document.getElementById('division').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/district',
                data: {division_id:division_id},
                success: function (data) {
                    document.getElementById('district').innerHTML = data;
                }
            });
        }

        function getUpazila() {
            var district_id = document.getElementById('district').value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:'/get/upazila',
                data: {district_id:district_id},
                success: function (data) {
                    document.getElementById('upazila').innerHTML = data;
                }
            });
        }
    </script>
@endsection












{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>--}}

{{--                                @if ($erro--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

{{--                                @if ($errors->has('email'))--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

{{--                                @if ($errors->has('password'))--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $errors->first('password') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
