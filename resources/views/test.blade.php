<!DOCTYPE html>
<html>

<head>
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Archivo|Montserrat);

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        /* Force HM to display emails at full width */

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        /* Force HM to display normal line spacing */

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */

        img {
            -ms-interpolation-mode: bicubic;
        }

        body {
            margin: 0;
            padding: 0;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        body,
        #bodyTable,
        #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        table {
            border-collapse: collapse;
        }

        img {
            display: block;
        }

        .appleLinks a {
            color: #8A959E !important;
            text-decoration: none;
        }

        span.preheader {
            display: none !important;
        }
    </style>

    <style type="text/css" id="hs-inline-css">
        .preheader,
        span[summary="preheader"] {
            display: none !important;
            Margin-left: -9999 !important;
            width: 0 !important;
            height: 0 !important;
            visibility: hidden !important;
            min-width: 0 !important;
            min-height: 0 !important;
            font-size: 0 !important;
            line-height: 0 !important;
        }

        .outer-table {
            width: 100%;
            max-width: 640px;
        }

        .inner-table {
            width: 93.75%;
            max-width: 600px;
        }

        .main-content {
            width: 83.34%;
            max-width: 500px;
        }

        .alt-content {
            width: 73.34%;
            max-width: 440px;
        }

        body {
            font-family: 'Montserrat', sans-serif;
            color: #8A959E;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: 'Montserrat', sans-serif;
            font-weight: 200;
            margin: 0 auto;
        }

        h1 {
            font-size: 40px;
            color: #1F2532;
            letter-spacing: 0;
        }

        h3 {
            font-size: 12px;
            color: #8A959E;
            letter-spacing: 1px;
        }

        p,
        li {
            font-family: 'Montserrat', sans-serif;
            font-weight: 300;
            margin: 0 0 1em 0;
            font-size: 18px;
            color: #8A959E;
            letter-spacing: 0;
            line-height: 38px;
        }

        p,
        ul,
        li,
        ol {
            text-align: left;
        }

        strong {
            font-weight: 500;
        }

        .main-content a {
            color: #FF3366 !important;
        }

        .main-content a:hover {
            color: #153DD1 !important;
        }

        .view-in-browser {
            font-size: 11px;
            color: #8A959E;
            letter-spacing: 0;
            border-bottom: 1px solid #B9C0C4;
            text-decoration: none;
        }

        .offer-table {
            background: url(https://ams3.digitaloceanspaces.com/collegify-storage-server/images/logo.svg) #464D5D center center / 640px 500px no-repeat !important;
            box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.15) !important;
            -webkit-box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.15) !important;
        }


        img + div {
            display: none;
        }
    </style>

    <style type="text/css" id="no-inline-css">
        @media screen and (max-width: 9999px) {
            .main-cta:hover,
            * [summary="main-cta"]:hover {
                background-color: #153DD1 !important;
                box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
                -webkit-box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
            }

            .twitter:hover img,
            * [summary="twitter"]:hover img {
                background-color: #153DD1 !important;
                box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
                -webkit-box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
            }

            .facebook:hover img,
            * [summary="facebook"]:hover img {
                background-color: #153DD1 !important;
                box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
                -webkit-box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
            }

            .linkedin:hover img,
            * [summary="linkedin"]:hover img {
                background-color: #153DD1 !important;
                box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
                -webkit-box-shadow: 0px 20px 40px -10px rgba(0, 0, 0, 0.2) !important;
            }


        }

        @media only screen and (max-width: 599px) {
        }

        @media only screen and (max-width: 480px) {
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
                -webkit-text-size-adjust: none !important;
            }

            /* Prevent Webkit platforms from changing default text sizes */
            body {
                width: 100% !important;
                min-width: 100% !important;
            }

            /* Prevent iOS Mail from adding padding to the body */
            .main-content,
            .author-content {
                width: 85% !important;
            }
        }
    </style>
</head>

<body bgcolor="#eaeced" style="font-family:'Montserrat', sans-serif; color:#8a959e">

<table id="body" class="body" summary="body" width="100%" height="100%" align="center" border="0" cellspacing="0"
       cellpadding="0" style="background-color:#eaeced;">
    <tbody>
    <tr>
        <td align="center" valign="top">

            <!-- START LIQUID WRAPPER -->
            <!--[if mso]>
            <table cellpadding="0" cellspacing="0" border="0" style="padding:0px;margin:0px;width:100%;">
                <tr>
                    <td style="padding:0px;margin:0px;">&nbsp;</td>
                    <td style="padding:0px;margin:0px;" width="640">
            <![endif]-->

            <table class="outer-table" summary="outer-table" align="center" valign="top" border="0" cellpadding="0"
                   cellspacing="0" style="width:100%; max-width:640px; Margin:0 auto" width="100%">

                <tbody>
                <tr>
                    <td>
                        <table class="inner-table" summary="inner-table" align="center" valign="top" border="0"
                               cellpadding="0" cellspacing="0"
                               style="width:93.75%; max-width:600px; Margin:0 auto; height:120px" height="120"
                               width="93.75%">
                            <tbody>
                            <tr height="25">
                                <td style="font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right" valign="middle"><a class="view-in-browser" href=""
                                                                     style="font-size:11px; color:#8a959e; letter-spacing:0; border-bottom:1px solid #b9c0c4; text-decoration:none"
                                                                     data-hs-link-id="0" target="_blank">View in
                                        browser</a></td>
                            </tr>

                            <tr height="8" style="font-size: 0; line-height: 0;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table class="master-table" width="900">

                                        <tbody>
                                        <tr>
                                            <td align="center">
                                                <table class="responsive-table" width="570" border="0" cellpadding="0"
                                                       cellspacing="0" valign="top">
                                                    <tbody>
                                                    <tr>

                                                        <td width="70"><a href="http://www.edexcourier.com"
                                                                          data-hs-link-id="0"><img
                                                                        src="https://image.ibb.co/iA53P9/Collegify_favicon.png"
                                                                        width="50.0" height="47.0" alt="InVision App"
                                                                        style="border:0;"></a></td>


                                                        <td class="responsive-header-cell-big"
                                                            style="font-family:'Open Sans', arial, sans-serif !important;font-size:25px;line-height:30px !important;font-weight:200 !important;color:#252b33 !important;">
                                                            Welcome To EDEX Courier
                                                        </td>


                                                        <td class="responsive-header-cell"
                                                            style="font-family:'Open Sans', arial, sans-serif !important;font-size:13px !important;line-height:30px !important;font-weight:400 !important;color:#7e8890 !important;text-transform:uppercase !important;"
                                                            align="right"> {{ date("d-M-Y") }}</td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="40">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table class="responsive-table" width="100%" bgcolor="#ffffff" border="0"
                                                       cellpadding="0" cellspacing="0" valign="top"
                                                       style="overflow:hidden !important;border-radius:3px;">
                                                    <tbody>
                                                    <tr>
                                                        <td><a href="https://image.ibb.co/mEeijU/webinar.png"
                                                               data-hs-link-id="0"><img
                                                                        src="hhttps://image.ibb.co/mEeijU/webinar.png?width=&amp;t=1458760852958&amp;width=580"
                                                                        class="hero-image" width="580"
                                                                        style="border: 0; max-width: 100% !important;"
                                                                        srcset="https://image.ibb.co/mEeijU/webinar.png"
                                                                        sizes="(max-width: 580px) 100vw, 580px"></a>
                                                        </td>
                                                    </tr>
                                                    <tr height="46">
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td align="center">
                                                            <table width="85%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center"><h2
                                                                                style="margin: 0 !important; font-family:'Archivo', sans-serif !important;font-size:28px !important;line-height:38px !important;font-weight:200 !important;color:#252b33 !important;">
                                                                            Confirmation !</h2></td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr height="25">
                                                        <td>&nbsp;</td>
                                                    </tr>


                                                    <tr>
                                                        <td align="center">
                                                            <table class="story-4" border="0" cellpadding="0"
                                                                   cellspacing="0" width="78%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center"
                                                                        style="font-family:'Montserrat', sans-serif !important; margin-bottom: 30px; font-size:16px !important;line-height:30px !important;font-weight:400 !important;color:#7e8890 !important;">
                                                                        Hello {{ $order->user->name }}! We have got
                                                                        your {{ count($order->order_details) }}
                                                                        Product(s). All details is given bellow of your
                                                                        products

                                                                        <table cellpadding="5" cellspacing="0"
                                                                               border="1" width="100%">
                                                                            <tr>
                                                                                <td>SL</td>
                                                                                <td>Tracking ID</td>
                                                                                <td>Receiver</td>
                                                                                <td>Receiver</td>
                                                                                <td>Price</td>
                                                                            </tr>
                                                                            @php($i = 1)
                                                                            @foreach($order->order_details as $product)
                                                                                <tr>
                                                                                    <td>{{ $i++ }}</td>
                                                                                    <td>{{ $product->tracking_id }}</td>
                                                                                    <td>{{ $product->receiver_name }}</td>
                                                                                    <td>{{ $product->receiver_phone }}</td>
                                                                                    <td>{{ $product->total_amount }}/-</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </table>
                                                                    </td>


                                                                </tr>
                                                                </tbody>
                                                            </table>


                                                        </td>
                                                    </tr>
                                                    <tr height="36">
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr height="20" class="last-tr-height">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr height="20">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>

                                <td>
                                    <table class="alt-content" summary="alt-content" align="center" valign="top"
                                           border="0" cellpadding="0" cellspacing="0"
                                           style="width:73.34%; max-width:440px; Margin:0 auto" width="73.34%">
                                        <tbody>
                                        <tr height="10">
                                            <td style="font-size:0; line-height:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><a href="http://collegify.com"
                                                                               data-hs-link-id="1" target="_blank">

                                                    <img src="https://image.ibb.co/iA53P9/Collegify_favicon.png"
                                                         width="30" height="30" alt="InVision"
                                                         style="border:0; width:30px;height:30px;">

                                                </a></td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"
                                                style="font-family: 'Montserrat', sans-serif; font-weight: 300; color:#1F2532;font-size:12px;letter-spacing:0;">
                                                <em></em>
                                            </td>
                                        </tr>


                                        <tr height="30">
                                            <td valign="top" align="center"
                                                style="font-family: 'Montserrat', sans-serif; font-weight: 300; color:#8A959E;font-size:12px;letter-spacing:0;vertical-align: bottom;">
                                                <span class="appleLinks">Copyright ©{{ date("Y") }} EDEX Courier. All rights reserved.</span>
                                            </td>
                                        </tr>
                                        <tr height="10">
                                            <td style="font-size:0; line-height:0;">&nbsp;</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr height="185">
                    <td>&nbsp;</td>
                </tr>

                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <td style="padding:0px;margin:0px;">&nbsp;</td>
            </tr>
            </table>
            <![endif]-->
            <!-- END LIQUID WRAPPER -->

        </td>
    </tr>
    </tbody>
</table>

</body>

</html>