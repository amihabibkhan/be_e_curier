
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>E-curier User Registration</title>

    <!-- Icons font CSS-->
    <link href="{{ asset('Frontend/registration_form') }}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('Frontend/registration_form') }}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{ asset('Frontend/registration_form') }}/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('Frontend/registration_form') }}/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('Frontend/registration_form') }}/css/main.css" rel="stylesheet" media="all">
</head>

<body>
@php
$boy = $merchant = false;
if (Request()->segment(2)){
    $boy = true;
}else{
    $merchant = true;
}
@endphp

<div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
    <div class="wrapper wrapper--w960">
        <div class="card card-2">
            <div class="card-heading"></div>
            <div class="card-body">
                <h2 class="title">{{ ($boy) ? "Delivery Boy" : "Merchant" }} Registration</h2>
                <form method="POST" action="{{ route('userRegistrationSubmit') }}" enctype="multipart/form-data">
                    @if($boy)
                        <input type="hidden" name="role_id" value="4">
                    @else
                        <input type="hidden" name="role_id" value="3">
                    @endif
                    @csrf
                    <div class="input-group">
                        <input class="input--style-2" type="text" value="{{ old('name') }}" placeholder="Name" name="name">
                        @if($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    @if($merchant)
                        <div class="input-group">
                            <input class="input--style-2" type="text" value="{{ old('company_name') }}" placeholder="Company Name" name="company_name">
                            @if($errors->has('company_name'))
                                <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('company_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    @endif
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-2" type="email" value="{{ old('email') }}" placeholder="E-mail Address" name="email">
                                @if($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red; font-style: italic">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-2" type="text" value="{{ old('phone') }}" placeholder="Phone Number" name="phone">
                                @if($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red; font-style: italic">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-2" type="password" placeholder="New Password" name="password">
                                @if($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-2" type="password" placeholder="Confirm Password" name="password_confirmation">
                                @if($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red; font-style: italic">{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="rs-select2 js-select-simple select--no-search">
                            <select name="branch_id">
                                <option disabled="disabled" selected="selected">Select a branch near you</option>
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}" {{ ($branch->id == old('branch_id') ? "Selected" : "") }}>{{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            <div class="select-dropdown"></div>
                        </div>
                        @if($errors->has('branch_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('branch_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <input class="input--style-2" type="text" value="{{ old('present_address') }}" placeholder="{{ ($merchant) ? "Office Location" : "Your Present Address" }}" name="present_address">
                        @if($errors->has('present_address'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('present_address') }}</strong>
                            </span>
                        @endif
                    </div>
                    @if($boy)
                        <div class="input-group">
                            <label for="" style="color: #666; font-weight: 500; font-size: 16px;">NID/Birth Certificate Scan Copy</label>
                            <input class="input--style-2" type="file" name="nid_image" style="margin-top: 5px">
                            @if($errors->has('nid_image'))
                                <span class="invalid-feedback" role="alert">
                                <strong style="color: red; font-style: italic">{{ $errors->first('nid_image') }}</strong>
                            </span>
                            @endif
                        </div>
                    @endif
                    <div class="p-t-30">
                        <button class="btn btn--radius btn--green" type="submit">Register</button>
                        @if($boy)
                            <a href="{{ route('userRegistrationForm') }}" class="btn btn--radius" style="text-decoration: none; background-color: #a74fff; margin-left: 10px">Register as Merchant</a>
                        @else
                            <a href="{{ route('deliveryBoyRegistrationForm') }}" class="btn btn--radius" style="text-decoration: none; background-color: #a74fff; margin-left: 10px">Delivery Boy Registraion</a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery JS-->
<script src="{{ asset('Frontend/registration_form') }}/vendor/jquery/jquery.min.js"></script>
<!-- Vendor JS-->
<script src="{{ asset('Frontend/registration_form') }}/vendor/select2/select2.min.js"></script>
<script src="{{ asset('Frontend/registration_form') }}/vendor/datepicker/moment.min.js"></script>
<script src="{{ asset('Frontend/registration_form') }}/vendor/datepicker/daterangepicker.js"></script>

<!-- Main JS-->
<script src="{{ asset('Frontend/registration_form') }}/js/global.js"></script>

</body>

</html>
