<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Auth::routes();
Route::get('/register', 'RegistrationController@userRegistrationForm')->name('userRegistrationForm');
Route::get('/register/delivery-boy', 'RegistrationController@userRegistrationForm')->name('deliveryBoyRegistrationForm');
Route::post('/register', 'RegistrationController@userRegistrationSubmit')->name('userRegistrationSubmit');

// ajax routes
Route::get('/get/district', 'UserController@getDistrict');
Route::get('/get/upazila', 'UserController@getUpazila');
Route::get('/get/tracking_id', 'UserController@product_tracking');


// admin routes
Route::get('/home', 'HomeController@index')->name('home')->middleware('suspend');
Route::get('/personal/profile', 'HomeController@personalProfile')->name('personalProfile')->middleware('suspend');
Route::get('/update/profile', 'HomeController@updateProfile')->name('updateProfile')->middleware('suspend');

Route::group(['prefix' => 'admin', 'middleware' => ['suspend']], function(){
    // Notifications
    Route::get('notifications', 'NotificationController@index')->name('notifications');
    Route::get('notification/{id}/delete', 'NotificationController@notificationDelete')->name('notificationDelete');

    // user update
    Route::get('/user/update/{id}', 'UserController@updateUserForm')->name('updateUserForm');
    Route::post('user/update', 'UserController@updateUser')->name('userRegistration.update');

    // single user view
    Route::get('/user/single/{id}/view', 'UserController@singleUserView')->name('singleUserView');

    Route::group(['middleware' => 'manager'], function(){
        // user registration
        Route::get('/user/registration/{type}', 'UserController@registrationForm')->name('registrationForm');
        Route::post('/user/registration', 'UserController@store')->name('userRegistration.store');

        // user view
        Route::get('/user/all/{type}/view', 'UserController@userList')->name('adminUser.list');

        // delivery boy payment
        Route::get('/payment/admin/history', 'IncomeController@adminPaymentHistory')->name('adminPaymentHistory');
        Route::post('/payment/admin/make', 'IncomeController@makePayment')->name('makePayment');
        Route::get('/payment/admin/{type}/{id}', 'IncomeController@changeOrDeletePaymentHistory')->name('changeOrDeletePaymentHistory');

        // merchant fee and payment
        Route::get('payment/merchant/history', 'MerchantTransactionController@merchantPaymentHistory')->name('merchantPaymentHistory');
        Route::get('payment/fee/history', 'MerchantTransactionController@merchantFeeHistory')->name('merchantFeeHistory');
        Route::get('payment/change/status/{type}/{id}', 'MerchantTransactionController@changeMerchantPaymentStatus')->name('changeMerchantPaymentStatus');
        Route::get('fee/change/status/{type}/{id}', 'MerchantTransactionController@changeMerchantFeeStatus')->name('changeMerchantFeeStatus');
        Route::post('payment/make/', 'MerchantTransactionController@makeMerchantPayment')->name('makeMerchantPayment');
        //fee
        Route::post('fee/make/', 'MerchantTransactionController@getDeliveryFeeFromMerchant')->name('getDeliveryFeeFromMerchant');

        // user suspend, delete and approve confirmation
        Route::get('/user/suspend/{id}/confirmation', 'UserController@userSuspendConfirmation')->name('userSuspendConfirmation');
        Route::get('/user/delete/{id}/confirmation', 'UserController@userDeleteConfirmation')->name('userDeleteConfirmation');
        Route::get('/user/approve/{id}/confirmation', 'UserController@userApproveConfirmation')->name('userApproveConfirmation');

        // admin order modify routes
        Route::get('/order/{type}/list', 'AdminOrderController@orderList')->name('orderListForAdmin');
        Route::post('/order/update/accept', 'AdminOrderController@acceptOrder')->name('acceptOrder');

        // admin delivery
        Route::get('/delivery/{type}/list', 'AdminDeliveryController@deliveryListForAdmin')->name('deliveryListForAdmin');
        Route::get('/products/change/{id}/{status_id}/status', 'AdminDeliveryController@changeStatus')->name('changeStatus');
        Route::post('assign/delivery/boy', 'AdminDeliveryController@assignForDeliver')->name('assignForDeliver');

        // resource routes
        Route::resource('area', 'AreaController');
        Route::resource('branch', 'BranchController');
        Route::resource('delivery_system', 'DeliverySystemController');
        Route::resource('cancel_reason', 'CancelController');
    });

    // merchant routes
    Route::group(['middleware'=> 'merchant', 'prefix' => 'merchant'], function (){
        Route::resource('order_request', 'OrderRequestController');
        Route::get('order/list/{type}', 'MerchantOrderController@merchantOrderList')->name('merchantOrderList');
        Route::get('order/change/status/{id}/{status}', 'MerchantOrderController@changeStatusByMerchant')->name('changeStatusByMerchant');

        // merchant payment list
        Route::get('payment/list', 'MerchantTransactionController@merchantPaymentList')->name('merchantPaymentList');
        Route::get('delivery/fee/payment/list', 'MerchantTransactionController@merchantDeliveryFeePaymentHistory')->name('merchantDeliveryFeePaymentHistory');
        Route::get('/accept/change/{type}/{id}', 'MerchantTransactionController@acceptOrRejectMerchantPayment')->name('acceptOrRejectMerchantPayment');
        Route::get('fee/change/{type}/{id}', 'MerchantTransactionController@acceptOrRejectMerchantDeliveryFee')->name('acceptOrRejectMerchantDeliveryFee');
    });

    // delivery boy routes
    Route::group(['prefix' => "boy"], function (){
       Route::get('order/{type}/list', 'BoyOrderController@orderList')->name('boy.orderList');
       Route::get('order/pick/{type}/{order_id}', 'BoyOrderController@acceptPickOrder')->name('boy.acceptPickOrder');
       Route::get('order/add/product/{order_id}', 'BoyOrderController@addProductsToOrder')->name('boy.addProductsToOrder');
       Route::post('order/add/product', 'BoyOrderController@addProductsToOrderSubmit')->name('boy.addProductsToOrderSubmit');
       Route::get('order/details/products/{id}', 'BoyOrderController@orderDetails')->name('boy.orderDetails');
       Route::get('order/delivered/office/{id}', 'BoyOrderController@deliveredToOffice')->name('boy.deliveredToOffice'); // notifyable

        // delivery routes for boy
        Route::get('delivery/products/{type}', 'BoyDeliveryController@boyDeliveryList')->name('boyDeliveryList');
        Route::get('delivery/request/accept/{id}', 'BoyDeliveryController@acceptDeliveryRequest')->name('acceptDeliveryRequest');
        Route::get('delivery/request/reject/{id}', 'BoyDeliveryController@rejectDeliveryRequest')->name('rejectDeliveryRequest');
        Route::get('delivery/collect/product/{id}', 'BoyDeliveryController@collectProductForDelivery')->name('collectProductForDelivery');
        Route::get('delivery/complete/confirm/{id}', 'BoyDeliveryController@requestToCompleteDelivery')->name('requestToCompleteDelivery');
        Route::post('delivery/cancel/', 'BoyDeliveryController@cancelOrderByBoy')->name('cancelOrderByBoy');

        // income and payment routes
        Route::get('/income/boy/history', 'IncomeController@boyIncomeHistory')->name('boyIncomeHistory');
        Route::get('/payment/boy/history', 'IncomeController@boyPaymentHistory')->name('boyPaymentHistory');
        Route::get('/payment/accept/boy/{type}/{id}', 'IncomeController@acceptPaymentRequest')->name('acceptPaymentRequest');
    });
});

// default pages (suspend and pending page)
Route::get('/account/suspend/', 'UserController@suspend')->name('userSuspend');
Route::get('/account/pending/', 'UserController@pending')->name('userPending');