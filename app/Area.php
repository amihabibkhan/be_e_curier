<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $guarded = [''];

    // relation with branch table
    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
}
