<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderDetails extends Model
{
    protected $guarded = [];

    // relation with order table
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    // relation with order status
    public function order_status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    // from branch
    public function fromBranch()
    {
        return $this->belongsTo('App\Branch', 'from_branch', 'id');
    }
    // from branch
    public function toBranch()
    {
        return $this->belongsTo('App\Branch', 'to_branch', 'id');
    }
    
    // delivery system
    public function delivery_system()
    {
        return $this->belongsTo(DeliverySystem::class);
    }

    // delivery payment system
    public function delivery_payment_system()
    {
        return $this->belongsTo(DeliveryPaymentSystem::class);
    }

    // relation with boy order
    public function boy_order()
    {
        return $this->hasOne('App\BoyOrder', 'order_id', 'id')->where('user_id', Auth::id());
    }


    // division table relation
    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    // district table relation
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    // upazila table relation
    public function upazila()
    {
        return $this->belongsTo(Upazila::class);
    }

    // area table relation
    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    
    // relation with cancel table
    public function cancel_reason()
    {
        return $this->belongsTo(CancelReason::class);
    }

    // merchant info
    public function merchant_info()
    {
        return $this->belongsTo(User::class, 'merchant_id');
    }
}
