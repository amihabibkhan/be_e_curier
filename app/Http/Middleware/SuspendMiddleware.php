<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuspendMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if(Auth::user()->approve_status_id == 1){
                return redirect(route('userPending'));
            }elseif(Auth::user()->approve_status_id == 3){
                return redirect(route('userSuspend'));
            }
        }else{
            return redirect(route('home'));
        }
        return $next($request);
    }
}
