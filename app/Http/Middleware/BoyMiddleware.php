<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BoyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if(Auth::user()->role_id != 4 || Auth::user()->role_id != 1){
                return abort(404);
            }
        }else{
            return redirect(route('home'));
        }
        return $next($request);
    }
}
