<?php

namespace App\Http\Controllers;

use App\BoyOrder;
use App\Notice;
use App\Order;
use App\PickerAssign;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminOrderController extends Controller
{
    // order list view
    public function orderList($id)
    {
        if (auth::user()->role_id == 1){
            if ($id == 'pending'){
                $data['orders'] = Order::where('main_order_status_id', 1)->get();
            }elseif($id == 'approved'){
                $data['orders'] = Order::where('main_order_status_id', 2)
                    ->orwhere('main_order_status_id', 3)
                    ->get();
            }elseif ($id == 'processing'){
                $data['orders'] = Order::where('main_order_status_id', 4)->get();
            }elseif ($id = 'completed'){
                $data['orders'] = Order::where('main_order_status_id', 5)->get();
            }
        }elseif(auth::user()->role_id == 2){
            if ($id == 'pending'){
                $data['orders'] = Order::where([['main_order_status_id', 1],['branch_id', auth::user()->branch_id]])->get();
            }elseif($id == 'approved'){
                $data['orders'] = Order::where([['main_order_status_id', 2],['branch_id', auth::user()->branch_id]])
                    ->orwhere([['main_order_status_id', 3], ['branch_id', auth::user()->branch_id]])
                    ->get();
            }elseif ($id == 'processing'){
                $data['orders'] = Order::where([['main_order_status_id', 4],['branch_id', auth::user()->branch_id]])->get();
            }elseif ($id = 'completed'){
                $data['orders'] = Order::where([['main_order_status_id', 5],['branch_id', auth::user()->branch_id]])->get();
            }
        }else{
            return abort(404);
        }

        $data['boys'] = User::where('role_id', 4)->get();
        $data['type'] = $id;
        return view('admin.order_management.orderList', $data);
    }

    // accept order and add a delivery boy
    public function acceptOrder(Request $request)
    {
        // validation
        if (!$request->delivery_boy){
            return back()->with('error', "Please, Select a delivery boy to pick this order");
        }

        if (PickerAssign::where([['order_id', $request->order_id], ['picker', $request->delivery_boy]])->exists()){
            PickerAssign::where([['order_id', $request->order_id], ['picker', $request->delivery_boy]])->delete();
        }

        // add to database
        PickerAssign::create([
           'order_id' => $request->order_id,
           'picker' => $request->delivery_boy,
           'assigner' => auth::id(),
           'boy_order_status_id' => 1
        ]);

        // order status change
        Order::find($request->order_id)->update(['main_order_status_id' => 2]);

        // notification
        Notice::insert([
            [
                'type' => 'Order Request Accepted',
                'details' => auth::user()->name . ' has accepted your order request',
                'user_id' => Order::find($request->order_id)->user_id,
                'created_at' => Carbon::now()
            ],[
                'type' => 'New Order to pick',
                'details' => 'You have selected to receive the order',
                'user_id' => $request->delivery_boy,
                'created_at' => Carbon::now()
            ]]);

        return back()->with('success', "Order request accepted successfully");
    }
}
