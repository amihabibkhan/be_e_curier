<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class RegistrationController extends Controller
{
    // registration form view
    public function userRegistrationForm()
    {
        $data['branches'] = Branch::all();
        return view('frontend.registerForm', $data);
    }

    // registration form submit
    public function userRegistrationSubmit(Request $request)
     {
         $request->validate([
             'name' => ['required', 'string', 'max:255'],
             'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
             'password' => ['required', 'string', 'min:6', 'confirmed'],
             'phone' => 'required|min:11|unique:users',
             'branch_id' => 'required',
             'role_id' => 'gt:2',
         ]);
         if ($request->role_id == 3){
             $request->validate([
                 'company_name' => 'required',
             ]);
         }
         if ($request->role_id == 4){
             $request->validate([
                 'nid_image' => 'required',
             ]);
         }

         // variable define
         $profile_pictue = '';
         $logo = '';
         $nid = '';

         // image upload
         if ($request->has('profile_pic')){
             $profile_pictue = $request->file('profile_pic')->store("profile_pic");
         }
         if ($request->has('company_logo')){
             $logo = $request->file('company_logo')->store("logo");
         }
         if ($request->has('nid_image')){
             $nid = $request->file('nid_image')->store("nid");
         }

         // data store to database

         User::create([
             'name' => $request->name,
             'email' => $request->email,
             'password' => Hash::make($request->password),
             'phone' => $request->phone,
             'role_id' => $request->role_id,
             'branch_id' => $request->branch_id,
             'profile_pic' => $profile_pictue,
             'company_logo' => $logo,
             'nid_image' => $nid,
             'designation' => $request->designation,
             'company_name' => $request->company_name,
             'fb_fan_page' => $request->fb_fan_page,
             'website' => $request->website,
             'division_id' => $request->division_id,
             'district_id' => $request->district_id,
             'upazila_id' => $request->upazila_id,
             'area_id' => $request->area_id,
             'nid_number' => $request->nid_number,
             'parmanent_address' => $request->parmanent_address,
             'present_address' => $request->present_address,
         ]);

         return redirect('/login')->with('success', "User created successfully");
     }
}
