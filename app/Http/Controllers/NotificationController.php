<?php

namespace App\Http\Controllers;

use App\Notice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    // all notification
    public function index()
    {
        if (auth::user()->role_id == 1){
            Notice::where('user_id', null)->update([
                'read_status_admin' => 1
            ]);
            $allNotifications = Notice::where('user_id', null)->orderBy('id', 'desc')->paginate(20);
        }elseif (auth::user()->role_id == 2){
            Notice::where([['user_id', null], ['branch_id', auth()->user()->branch_id]])->update([
                'read_status_manager' => 1
            ]);
            $allNotifications = Notice::where([['user_id', null], ['branch_id', auth()->user()->branch_id]])->orderBy('id', 'desc')->paginate(20);
        }else{
            Notice::where('user_id', auth::id())->update([
                'read_status_user' => 1
            ]);
            $allNotifications = Notice::where('user_id', auth::id())->orderBy('id', 'desc')->paginate(20);
        }
        return view('admin.notification.notificationList', compact('allNotifications'));
    }

    // delete single notification
    public function notificationDelete($id)
    {
        Notice::findOrFail($id)->delete();
        return back()->with('success', 'Notification Deleted Successfully');
    }
}
