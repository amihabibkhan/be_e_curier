<?php

namespace App\Http\Controllers;

use App\Area;
use App\BoyOrder;
use App\Branch;
use App\DeliveryPaymentSystem;
use App\DeliverySystem;
use App\Division;
use App\IncomeHistory;
use App\Notice;
use App\Notifications\MerchantNotification;
use App\Order;
use App\OrderDetails;
use App\PickerAssign;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class BoyOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('manager')->only('deliveredToOffice');
    }


    // order list
    public function orderList($type)
    {
        if ($type == 'pending'){
            $data['orders'] = User::find(auth::id())->pending_pick_orders;
        }elseif($type == 'rejected'){
            $data['orders'] = User::find(auth::id())->rejected_pick_orders;
        }elseif($type == 'accepted'){
            $data['orders'] = User::find(auth::id())->accepted_pick_orders;
        }elseif($type == 'completed'){
            $data['orders'] = User::find(auth::id())->completed_pick_orders;
        }
        $data['type'] = $type;
        return view('admin.delivery_boy.pick.orderList', $data);
    }

    // accept or reject pick order
    public function acceptPickOrder($type, $order_id)
    {
        if ($type == 'accept'){
            PickerAssign::where([['order_id', $order_id], ['picker', Auth::id()]])->update([
               'boy_order_status_id' => 2
            ]);
            $message = "Order accepted successfully";

            // notification
            Notice::insert([
                    'type' => 'Order Pick Request Accepted',
                    'details' => auth::user()->name . ' has accepted order pick request',
                    'branch_id' => auth::user()->branch_id,
                    'created_at' => Carbon::now()
                ]);
        }elseif ($type == 'reject'){
            PickerAssign::where([['order_id', $order_id], ['picker', Auth::id()]])->update([
                'boy_order_status_id' => 3
            ]);

            // order status change
            Order::find($order_id)->update(['main_order_status_id' => 1]);

            $message = "Order rejected successfully";

            // notification
            Notice::insert([
                'type' => 'Order Pick Request Rejected',
                'details' => auth::user()->name . ' has rejected order pick request',
                'branch_id' => auth::user()->branch_id,
                'created_at' => Carbon::now()
            ]);
        }
        return back()->with('success', $message);
    }

    // add product to order form view
    public function addProductsToOrder($id)
    {
        $data['areas'] = Area::all();
        $data['branches'] = Branch::all();
        $data['divisions'] = Division::all();
        $data['delivery_systems'] = DeliverySystem::all();
        $data['delivery_payment_systems'] = DeliveryPaymentSystem::all();
        $data['order_id'] = $id;
        return view('admin.delivery_boy.pick.addProduct', $data);
    }

    // product add to order form submit
    public function addProductsToOrderSubmit(Request $request)
    {
        $request->validate([
            'order_id' => 'required',
            'to_branch' => 'required',
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
            'division_id' => 'required',
            'district_id' => 'required',
            'upazila_id' => 'required',
            'delivery_system_id' => 'required',
            'delivery_payment_system_id' => 'required',
        ]);
        // if not check the check box of confirm payment
        if ($request->delivery_payment_system_id == 2 && !$request->confirm_delivery_fee){
            return back()->with('error', "Please, Confirm delivery fee if its a prepaid payment order");
        }

        // for first product change status and add to BoyOrder Pivot Table
        if (!OrderDetails::where('order_id', $request->order_id)->exists()){

            // its doesn't need because we are using two tables for delivery and pick
//            BoyOrder::create([
//               'order_id' => $request->order_id,
//               'user_id' => Auth::id(),
//               'branch_id' => Auth::user()->branch_id,
//               'boy_order_status_id' => 2,
//               'order_type_id' => 1,
//            ]);

            Order::find($request->order_id)->update([
                'main_order_status_id' => 3
            ]);
        }

        // inserting info to database
        $order_details = new OrderDetails();
        $order_details->order_id = $request->order_id;
        $order_details->merchant_id = Order::find($request->order_id)->user_id;
        $order_details->from_branch = Order::find($request->order_id)->branch_id;
        $order_details->to_branch = $request->to_branch;
        $order_details->product_description = $request->product_description;
        $order_details->receiver_name = $request->receiver_name;
        $order_details->receiver_phone = $request->receiver_phone;
        $order_details->division_id = $request->division_id;
        $order_details->district_id = $request->district_id;
        $order_details->upazila_id = $request->upazila_id;
        $order_details->area_id = $request->area_id;
        $order_details->total_amount = $request->total_amount;

        // delivery fee
        $system = DeliverySystem::find($request->delivery_system_id);
        $delivery_fee = $system->receiver_boy_fee;
        $delivery_fee += $system->delivery_boy_fee;
        $delivery_fee += $system->company_fee;

        // delivery fee insert to database
        $order_details->delivery_fee = $delivery_fee;
        $order_details->delivery_system_id = $request->delivery_system_id;

        // if its a prepaid order it will must be paid of its delivery fee
        if ($request->delivery_payment_system_id == 2){
            $order_details->delivery_fee_payment_status = 1;
        }
        $order_details->delivery_payment_system_id = $request->delivery_payment_system_id;
        $order_details->tracking_id = '';
        $order_details->created_at = Carbon::now();
        $order_details->save();

        $order_details->tracking_id = "ECRPD" . date("ymd") . $order_details->id;
        $order_details->save();

        // sms to client

        $url = "http://66.45.237.70/api.php";
        $number="{$order_details->receiver_phone}";
        $text="Dear " . $order_details->receiver_name . ",
Your product has been taken from " . Order::find($order_details->order_id)->user->company_name . ".
You can trace your product status by this ID: '" . $order_details->tracking_id . "' at edexcourier.com

Thank You.";
        $data= array(
            'username'=>"01712794033",
            'password'=>"strong@787664",
            'number'=>"$number",
            'message'=>"$text"
        );

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $smsresult = curl_exec($ch);
        $p = explode("|",$smsresult);
        $sendstatus = $p[0];

        return back()->with('success', "Product added to order successfully");
    }

    // order details page
    public function orderDetails($id)
    {
        $data['order_info'] = Order::find($id);
        $data['products'] = OrderDetails::where('order_id', $id)->get();
        return view('admin.delivery_boy.pick.order_details', $data);
    }

    // product delivered to office confirmation
    public function deliveredToOffice($id)
    {
        $order = Order::find($id);
        // update order status
        $order->update([
            'main_order_status_id' => 4
        ]);

        // getting delivery boy id (it must be before PickerAssign Update)
        $delivery_boy = PickerAssign::where([
            ['boy_order_status_id', 2],
            ['order_id', $id]
        ])->first()->picker;

        // update picker assign status
        PickerAssign::where([['boy_order_status_id' ,2], ['order_id', $id]])->update([
            'boy_order_status_id' => 5
        ]);

        // counting total income of delivery boy from this order
        $total_income = 0;

        // add income history to delivery boy account
        foreach ($order->order_details as $single_order){
            IncomeHistory::create([
                'order_detail_id' => $single_order->id,
                'boy_id' => $delivery_boy,
                'order_type_id' => 1,
                'amount' => $single_order->delivery_system->receiver_boy_fee,
            ]);
            $total_income += $single_order->delivery_system->receiver_boy_fee;

            // order_details status change to (arrived to office)
            $single_order->order_status_id = 2;
            $single_order->save();
        }

        // notification
        Notice::insert([
            [
                'type' => $total_income . ' TK Added to your account',
                'details' => $total_income . ' added to your account from receiving order - ' . $order->tracking_id,
                'user_id' => $delivery_boy,
                'created_at' => Carbon::now()
            ],[
                'type' => 'Your products reached to our office',
                'details' => 'Dear user, We have received you all products. We will deliver it to your client',
                'user_id' => $order->user_id,
                'created_at' => Carbon::now()
            ]]);

        $merchant = User::find($order->user_id);
        Notification::send($merchant, new MerchantNotification($order));
        return back()->with('success', "Product successfully delivered to office");
    }
}
