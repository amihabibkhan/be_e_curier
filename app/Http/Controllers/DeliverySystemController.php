<?php

namespace App\Http\Controllers;

use App\DeliverySystem;
use Illuminate\Http\Request;

class DeliverySystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['systems'] = DeliverySystem::all();
        return view('admin.delivery_system.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'system_details' => 'required',
           'receiver_boy_fee' => 'required',
           'delivery_boy_fee' => 'required',
           'company_fee' => 'required'
        ]);
        DeliverySystem::create([
            'system_details' => $request->system_details,
            'receiver_boy_fee' => $request->receiver_boy_fee,
            'delivery_boy_fee' => $request->delivery_boy_fee,
            'company_fee' => $request->company_fee
        ]);
        return back()->with('success', "New delivery system created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'system_details' => 'required',
            'receiver_boy_fee' => 'required',
            'delivery_boy_fee' => 'required',
            'company_fee' => 'required'
        ]);
        DeliverySystem::find($id)->update([
            'system_details' => $request->system_details,
            'receiver_boy_fee' => $request->receiver_boy_fee,
            'delivery_boy_fee' => $request->delivery_boy_fee,
            'company_fee' => $request->company_fee
        ]);
        return back()->with('success', "Delivery system Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
