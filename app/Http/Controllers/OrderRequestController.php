<?php

namespace App\Http\Controllers;

use App\Notice;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        if (Auth::user()->role_id == 1){
            $data['merchants'] = User::where('role_id', 3)->get();
        }elseif (Auth::user()->role_id == 2){
            $data['merchants'] = User::where(['role_id' => 3, 'branch_id' => Auth::user()->branch_id])->get();
        }
        return view('admin.order_request.request_form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'product_quantity' => 'required|gt:0',
           'weight' => 'required|gt:0'
        ]);
        $order = new Order();
        $order->user_id = $request->merchant_id;
        $order->branch_id = auth::user()->branch_id;
        $order->product_quantity = $request->product_quantity;
        $order->weight = $request->weight;
        $order->tracking_id = '';
        $order->created_at = Carbon::now();
        $order->save();
        $order->tracking_id = "ECR" . date("ymd") . $order->id;
        $order->save();

        // notification
        Notice::create([
           'type' => 'Order Request',
           'details' => auth::user()->name . ' has created an order request',
           'branch_id' => auth::user()->branch_id,
        ]);
        return back()->with('success', "Order request submitted successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 'pending'){
            $data['orders'] = Order::where([
                ['user_id', auth::id()],
                ['main_order_status_id', 1]
            ])->get();
        }elseif($id == 'processing'){
            $data['orders'] = Order::where([
                ['user_id', auth::id()],
                ['main_order_status_id', 2]
            ])->orwhere([
                ['user_id', auth::id()],
                ['main_order_status_id', 3]
            ])->orwhere([
                ['user_id', auth::id()],
                ['main_order_status_id', 4]
            ])->get();
        }elseif ($id = 'completed'){
            $data['orders'] = Order::where([
                ['user_id', auth::id()],
                ['main_order_status_id', 5]
            ])->get();
        }
        $data['type'] = $id;
        return view('admin.order_request.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_quantity' => 'required|gt:0',
            'weight' => 'required|gt:0'
        ]);
        if ($order->user_id != auth::id()){
            return back()->with('error', "Sorry! You haven't permission to delete this order");
        }
        $order = Order::find($id);
        $order->product_quantity = $request->product_quantity;
        $order->weight = $request->weight;
        $order->updated_at = Carbon::now();
        $order->save();

        return back()->with('success', "Order request updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if ($order->user_id != auth::id()){
            return back()->with('error', "Sorry! You haven't permission to delete this order");
        }
        $order->delete();
        return back()->with('success', "Order Request Deleted Successfully");
    }
}
