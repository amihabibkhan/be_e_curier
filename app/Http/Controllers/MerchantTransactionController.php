<?php

namespace App\Http\Controllers;

use App\MerchantFee;
use App\MerchantPayment;
use App\PaymentHistory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MerchantTransactionController extends Controller
{
    // merchant boy payment history for admin
    public function merchantPaymentHistory()
    {
        if (Auth::user()->role_id == 1){

            $data['merchants'] = User::where('role_id', 3)->get();
            $data['payments'] = MerchantPayment::all();
            $data['total_payment'] = MerchantPayment::where('status', 1)->sum('amount');

        }elseif (Auth::user()->role_id == 2){

            $data['merchants'] = User::where([
                ['role_id', 3],
                ['branch_id', Auth::user()->branch_id],
            ])->get();
            $data['payments'] = MerchantPayment::where('paid_by', Auth::id())->get();
            $data['total_payment'] = MerchantPayment::where([
                ['paid_by', Auth::id()],
                ['status', 1]
            ])->sum('amount');
        }
        return view('admin.merchant.payment', $data);
    }
    


    // change payment status or delete
    public function changeMerchantPaymentStatus($type, $id)
    {
        $payment = MerchantPayment::find($id);
        if ($type == 're-payment'){
            $payment->update([
                'status' => 0
            ]);
        }else{
            if ($payment->status != 1){
                $payment->delete();
            }else{
                return back()->with('error', "Already Completed this payment");
            }
        }
        return back()->with('success', "Payment Status Changed Successfully");
    }

    // new payment
    public function makeMerchantPayment(Request $request)
    {
        $request->validate([
            'merchant_id' => 'required',
            'amount'=> 'required|numeric',
        ]);

        // check if payment amount is greater then balance
        if ($request->available_balance < $request->amount){
            return back()->with('error', "Insufficient Balance. Please Check");
        }

        MerchantPayment::create([
            'amount' => $request->amount,
            'merchant_id' => $request->merchant_id,
            'paid_by' => Auth::id(),
            'note' => $request->note,
        ]);

        return back()->with('success', "Payment Completed Successfully");
    }

    // payment list for merchant (Merchant Methods start from here) =========================================
    public function merchantPaymentList()
    {
        $data['payments'] = MerchantPayment::where('merchant_id', Auth::id())->get();
        $data['total_payment'] = MerchantPayment::where('status', 1)->sum('amount');
        return view('admin.merchant.payment', $data);
    }

    // accept or reject payment by merchant
    public function acceptOrRejectMerchantPayment($type, $id)
    {
        $payment = MerchantPayment::findOrFail($id);
        if ($payment->merchant_id != Auth::id()){
            return back()->with('error', 'Sorry! You have not enough access to do this action');
        }
        if ($type == 'accept'){
            $payment->status = 1;
        }else{
            $payment->status = 2;
        }
        $payment->save();
        return back()->with('success', 'Payment Status Changed Successfully');
    }



    // ============= Merchant Fee methods start from here =======================

    // merchant fee history for admin
    public function merchantFeeHistory()
    {
        if (Auth::user()->role_id == 1){

            $data['merchants'] = User::where('role_id', 3)->get();
            $data['payments'] = MerchantFee::all();
            $data['total_payment'] = MerchantFee::where('status', 1)->sum('amount');

        }elseif (Auth::user()->role_id == 2){

            $data['merchants'] = User::where([
                ['role_id', 3],
                ['branch_id', Auth::user()->branch_id],
            ])->get();
            $data['payments'] = MerchantFee::where('paid_to', Auth::id())->get();
            $data['total_payment'] = MerchantFee::where([
                ['paid_to', Auth::id()],
                ['status', 1]
            ])->sum('amount');
        }
        return view('admin.merchant.fee', $data);
    }


    // get delivery fee from merchant form submit
    public function getDeliveryFeeFromMerchant(Request $request)
    {
        $request->validate([
            'merchant_id' => 'required',
            'amount'=> 'required|numeric',
        ]);

        MerchantFee::create([
            'amount' => $request->amount,
            'merchant_id' => $request->merchant_id,
            'paid_to' => Auth::id(),
            'note' => $request->note,
        ]);

        return back()->with('success', "Payment Completed Successfully");
    }

    // delivery fee payment list
    public function merchantDeliveryFeePaymentHistory()
    {
        $data['payments'] = MerchantFee::where('merchant_id', Auth::id())->get();
        $data['total_payment'] = MerchantFee::where([
            ['merchant_id', Auth::id()],
            ['status', 1]
        ])->sum('amount');
        return view('admin.merchant.fee', $data);
    }


    // accept or reject delivery fee payment
    public function acceptOrRejectMerchantDeliveryFee($type, $id)
    {
        $payment = MerchantFee::findOrFail($id);
        if ($payment->merchant_id != Auth::id()){
            return back()->with('error', 'Sorry! You have not enough access to do this action');
        }
        if ($type == 'accept'){
            $payment->status = 1;
        }else{
            $payment->status = 2;
        }
        $payment->save();

        return back()->with('success', 'Payment Status Changed Successfully');
    }

    // delete or re-payment
    public function changeMerchantFeeStatus($type, $id)
    {
        $payment = MerchantFee::find($id);
        if ($type == 're-payment'){
            $payment->update([
                'status' => 0
            ]);
        }else{
            if ($payment->status != 1){
                $payment->delete();
            }else{
                return back()->with('error', "Already Completed this payment");
            }
        }
        return back()->with('success', "Payment Status Changed Successfully");
    }

}
