<?php

namespace App\Http\Controllers;

use App\BoyOrder;
use App\IncomeHistory;
use App\Notice;
use App\Order;
use App\OrderDetails;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminDeliveryController extends Controller
{
    // delivery list for admin
    public function deliveryListForAdmin($id)
    {
        // $id means type

        if (auth::user()->role_id == 1){

            if ($id == 'pending-for-send'){

                $data['products'] = OrderDetails::where('order_status_id', 2)->get();

            }elseif($id == 'pending-to-receive'){

                $data['products'] = OrderDetails::where('order_status_id', 3)->get();

            }elseif ($id == 'arrived-to-office'){

                $data['products'] = OrderDetails::where('order_status_id', 4)->get();

            }elseif ($id == 'taken-by-boy'){

                $data['products'] = OrderDetails::where('order_status_id', 5)->get();

            }elseif ($id == 'on-the-way'){

                $data['products'] = OrderDetails::where('order_status_id', 6)->get();

            }elseif ($id == 'requested-for-complete'){

                $data['products'] = OrderDetails::where('order_status_id', 8)->get();

            }elseif ($id == 'completed'){

                $data['products'] = OrderDetails::where('order_status_id', 7)->get();

            }elseif ($id == 'cancelled'){

                $data['products'] = OrderDetails::where('order_status_id', 9)->get();

            }elseif ($id == 'requested-to-returned'){

                $data['products'] = OrderDetails::where('order_status_id', 10)->get();

            }elseif ($id == 'returned-to-office'){

                $data['products'] = OrderDetails::where('order_status_id', 11)->get();

            }elseif ($id == 'send-to-merchant'){

                $data['products'] = OrderDetails::where('order_status_id', 12)->get();

            }elseif ($id == 'received-by-merchant'){

                $data['products'] = OrderDetails::where('order_status_id', 13)->get();

            }

        }elseif(auth::user()->role_id == 2){

            if ($id == 'pending-for-send'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 2],
                    ['from_branch', Auth::user()->branch_id],
                ])->get();

            }elseif($id == 'pending-to-receive'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 3],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'arrived-to-office'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 4],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'taken-by-boy'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 5],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'on-the-way'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 6],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'requested-for-complete'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 8],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'completed'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 7],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'cancelled'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 9],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'requested-to-returned'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 10],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'returned-to-office'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 11],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'send-to-merchant'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 12],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }elseif ($id == 'received-by-merchant'){

                $data['products'] = OrderDetails::where([
                    ['order_status_id', 13],
                    ['to_branch', Auth::user()->branch_id],
                ])->get();

            }

        }else{
            return abort(404);
        }

        $data['type'] = $id;
        $data['boys'] = User::where('role_id', 4)->get();
        return view('admin.delivery_management.deliveryOrderList', $data);
    }

    // transfer to branch
    public function changeStatus($id, $change_type)
    {

        // add a security layer here
        $order_details = $product = OrderDetails::find($id);
        if ($change_type == 7){
            $delivery_boy = BoyOrder::where([['order_id', $id],['boy_order_status_id', 7]])->first();

            BoyOrder::where([
                ['order_id', $id],
                ['boy_order_status_id', 7]
            ])->update([
                'boy_order_status_id' => 5
            ]);

            // add delivery fee to delivery boy
            IncomeHistory::create([
                'order_detail_id' => $id,
                'boy_id' => $delivery_boy->user_id,
                'order_type_id' => 2,
                'amount' => $product->delivery_system->delivery_boy_fee,
            ]);

            // notification
            Notice::insert([
                'type' => $product->delivery_system->delivery_boy_fee . 'TK added to your account',
                'details' => 'You have earned from your delivery',
                'user_id' => $delivery_boy->user_id,
                'created_at' => Carbon::now()
            ]);
        }

        $product->order_status_id = $change_type;
        $product->save();


        // sms notification for success message
        if ($change_type == 7){
            $url = "http://66.45.237.70/api.php";
            $number="{$order_details->merchant_info->phone}";
            $text="Dear " . $order_details->merchant_info->name . ",
Your product '" . $order_details->tracking_id . "' has been delivered successfully.

Thank You.";
            $data= array(
                'username'=>"01712794033",
                'password'=>"strong@787664",
                'number'=>"$number",
                'message'=>"$text"
            );

            $ch = curl_init(); // Initialize cURL
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $smsresult = curl_exec($ch);
            $p = explode("|",$smsresult);
            $sendstatus = $p[0];
        }

        // sms for cancel message
        if ($change_type == 10){
            $url = "http://66.45.237.70/api.php";
            $number="{$order_details->merchant_info->phone}";
            $text="Dear " . $order_details->merchant_info->name . ",
Your product '" . $order_details->tracking_id . "' has been canceled. Please contact with your client.

Thank You.";
            $data= array(
                'username'=>"01712794033",
                'password'=>"strong@787664",
                'number'=>"$number",
                'message'=>"$text"
            );

            $ch = curl_init(); // Initialize cURL
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $smsresult = curl_exec($ch);
            $p = explode("|",$smsresult);
            $sendstatus = $p[0];
        }
        return back()->with('success', 'Action successful');
    }

    // assign a delivery boy
    public function assignForDeliver(Request $request)
    {
        $request->validate([
           'product_id' => 'required',
           'delivery_boy' => 'required',
        ]);

        if (BoyOrder::where([['order_id', $request->product_id],['user_id', $request->delivery_boy]])->exists()){
            BoyOrder::where([['order_id', $request->product_id],['user_id', $request->delivery_boy]])->delete();
        }


        BoyOrder::create([
           'order_id' => $request->product_id,
           'user_id' => $request->delivery_boy,
           'branch_id' => User::find($request->delivery_boy)->branch_id,
           'boy_order_status_id' => 1,
           'order_type_id' => 2,
        ]);

        // change order status
        OrderDetails::find($request->product_id)->update([
           'order_status_id' => 5
        ]);

        // notification
        Notice::insert([
            'type' => 'New Delivery Order',
            'details' => 'You are selected to deliver a new product.',
            'user_id' => $request->delivery_boy,
            'created_at' => Carbon::now()
        ]);

        return back()->with('success', 'Delivery boy assigned successfully');
    }
}
