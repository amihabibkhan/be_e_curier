<?php

namespace App\Http\Controllers;

use App\Area;
use App\Branch;
use App\District;
use App\Division;
use App\Order;
use App\OrderDetails;
use App\Upazila;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('manager')->only('userApproveConfirmation','userSuspendConfirmation','registrationForm', 'userList', 'userDeleteConfirmation');
    }

    // user registration form
    public function registrationForm($type)
    {
        $data['areas'] = Area::all();
        $data['divisions'] = Division::all();
        $data['branches'] = Branch::all();
        $data['type'] = $type;
        return view('auth.registration', $data);
    }

    // registration form store
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role_id' => 'required',
            'branch_id' => 'required',
            'phone' => 'required|min:11|unique:users',
        ]);

        // user role check
        if ($request->role_id != 4 && $request->role_id != 3){
            if (Auth::check()){
                if(Auth::user()->role_id != 2 && Auth::user()->role_id != 1){
                    return abort(404);
                }
            }else{
                return redirect(route('home'));
            }
        }

        // variable define
        $profile_pictue = '';
        $logo = '';
        $nid = '';

        // image upload
        if ($request->has('profile_pic')){
            $profile_pictue = $request->file('profile_pic')->store("profile_pic");
        }
        if ($request->has('company_logo')){
            $logo = $request->file('company_logo')->store("logo");
        }
        if ($request->has('nid_image')){
            $nid = $request->file('nid_image')->store("nid");
        }

        // data store to database

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => $request->role_id,
            'branch_id' => $request->branch_id,
            'profile_pic' => $profile_pictue,
            'company_logo' => $logo,
            'nid_image' => $nid,
            'designation' => $request->designation,
            'company_name' => $request->company_name,
            'fb_fan_page' => $request->fb_fan_page,
            'website' => $request->website,
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'upazila_id' => $request->upazila_id,
            'area_id' => $request->area_id,
            'nid_number' => $request->nid_number,
            'parmanent_address' => $request->parmanent_address,
            'present_address' => $request->present_address,
        ]);

        return back()->with('success', "User created successfully");
    }

    // user list view for admin
    public function userList($type)
    {
        if ($type == 'manager'){
            $data['users'] = User::where([
                ['role_id', 2],
                ['approve_status_id', 2]
            ])->get();
            return view('admin.userlist.manager', $data);
        }
        if ($type == 'merchant'){
            if(auth::user()->role_id == 1){
                $data['users'] = User::where([
                    ['role_id', 3],
                    ['approve_status_id', 2]
                ])->get();
                return view('admin.userlist.merchant', $data);
            }else{
                $data['users'] = User::where([
                    ['role_id', 3],
                    ['approve_status_id', 2],
                    ['branch_id', auth::user()->branch_id]
                ])->get();
                return view('admin.userlist.merchant', $data);
            }
        }

        if($type == 'delivery-boy'){
            if(auth::user()->role_id == 1){
                $data['users'] = User::where([
                    ['role_id', 4],
                    ['approve_status_id', 2]
                ])->get();
                return view('admin.userlist.boy', $data);
            }else{
                $data['users'] = User::where([
                    ['role_id', 4],
                    ['approve_status_id', 2],
                    ['branch_id', auth::user()->branch_id]
                ])->get();
                return view('admin.userlist.boy', $data);
            }
        }

        if($type == 'pending'){
            if (auth::user()->role_id == 1){
                $data['users'] = User::where('approve_status_id', 1)->get();
                return view('admin.userlist.pending', $data);
            }else{
                $data['users'] = User::where([
                    ['approve_status_id', 1],
                    ['branch_id', auth::user()->branch_id]
                ])->get();
                return view('admin.userlist.pending', $data);
            }
        }
        if($type == 'suspended'){
            if (auth::user()->role_id == 1){
                $data['users'] = User::where('approve_status_id', 3)->get();
                return view('admin.userlist.suspend', $data);
            }else{
                $data['users'] = User::where([
                    ['approve_status_id', 3],
                    ['branch_id', auth::user()->branch_id]
                ])->get();
                return view('admin.userlist.suspend', $data);
            }
        }
        return abort(404);
    }


    // user info update form
    public function updateUserForm($id)
    {
        $data['areas'] = Area::all();
        $data['divisions'] = Division::all();
        $data['districts'] = District::where('division_id', User::find($id)->division_id)->get();
        $data['upazilas'] = Upazila::where('district_id', User::find($id)->district_id)->get();
        $data['branches'] = Branch::all();
        $data['user_info'] = User::findOrFail($id);
        $data['type'] = $data['user_info']->role->role;
        return view('admin.updateUser', $data);
    }

    // update user info
    public function updateUser(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => 'required|string|email|max:255|unique:users,email,' . $request->id,
            'phone' => 'required|min:11|unique:users,phone,' . $request->id,
        ]);

        $user = User::find($request->id);

        // image upload (if has)
        if ($request->has('profile_pic')){
            Storage::delete($user->profile_pic);
            $profile_pictue = $request->file('profile_pic')->store("profile_pic");
            $user->profile_pic = $profile_pictue;
        }
        if ($request->has('company_logo')){
            Storage::delete($user->company_logo);
            $logo = $request->file('company_logo')->store("logo");
            $user->company_logo = $logo;
        }
        if ($request->has('nid_image')){
            Storage::delete($user->nid_image);
            $nid = $request->file('nid_image')->store("nid");
            $user->nid_image = $nid;
        }

        // password if has
        if ($request->password){
            if ((auth::user()->role_id == 3 || auth::user()->role_id == 4) || auth::user()->id == $request->id){
                if (Hash::check($request->old_password, $user->password)){
                    $user->password = Hash::make($request->password);
                }else{
                    return back()->with('error', "Sorry, Old Password doesn't match");
                }
            }else{
                $user->password = Hash::make($request->password);
            }
        }

        // data store to database
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->role_id = $request->role_id;
        $user->branch_id = $request->branch_id;
        $user->designation = $request->designation;
        $user->company_name = $request->company_name;
        $user->fb_fan_page = $request->fb_fan_page;
        $user->website = $request->website;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->upazila_id = $request->upazila_id;
        $user->area_id = $request->area_id;
        $user->nid_number = $request->nid_number;
        $user->parmanent_address = $request->parmanent_address;
        $user->present_address = $request->present_address;
        $user->save();

        return back()->with('success', "User Updated successfully");
    }

    // single user details view
    public function singleUserView($id)
    {
        $data['user_info'] = User::findOrFail($id);
        return view('admin.userlist.singleView', $data);
    }

    // user suspend confirmation
    public function userSuspendConfirmation($id)
    {
        $user = User::findOrFail($id);
        if (auth::user()->role_id != 1 && auth::user()->branch_id != $user->branch_id){
            return back()->with('error', "Sorry, You haven't any permission to Suspend this user");
        }
        if ($user->approve_status_id == 3){
            $user->approve_status_id = 2;
            $message = "User Activated Successfully";
        }elseif ($user->approve_status_id == 2){
            $user->approve_status_id = 3;
            $message = "User Suspended Successfully";
        }
        $user->save();
        return back()->with('success', $message);
    }

    // user suspend confirmation
    public function userApproveConfirmation($id)
    {
        $user = User::findOrFail($id);
        if (auth::user()->role_id != 1 && auth::user()->branch_id != $user->branch_id){
            return back()->with('error', "Sorry, You haven't any permission to Approve this user");
        }
        $user->update([
            'approve_status_id' => 2
        ]);
        return back()->with('success', "User Approved Successfully");
    }


    // user Delete confirmation
    public function userDeleteConfirmation($id)
    {
        $user = User::findOrFail($id);
        if (auth::user()->role_id == 1 || auth::user()->branch_id == $user->branch_id){
            Storage::delete($user->profile_pic);
            Storage::delete($user->company_logo);
            Storage::delete($user->nid_image);
            $user->delete();
        }else{
            return back()->with('error', "Sorry, You haven't any permission to delete this user");
        }


        return back()->with('success', "User Deleted Successfully");
    }















    // suspend page view
    public function suspend()
    {
        return view('errors.suspend');
    }

    // suspend page view
    public function pending()
    {
        return view('errors.pending');
    }

    // get district by ajax
    public function getDistrict(Request $request)
    {
        $division_id = $request->division_id;
        $districts = District::where('division_id', $division_id)->get();
        $data = "<option disabled selected>Select a District</option>";
        foreach ($districts as $district){
            $data .= "<option value='". $district->id ."'>" . $district->name . "</option>";
        }
        return $data;
    }

    // get upazila by ajax
    public function getUpazila(Request $request)
    {
        $district_id = $request->district_id;
        $upazilas = Upazila::where('district_id', $district_id)->get();
        $data = "<option disabled selected>Select a Upazila</option>";
        foreach ($upazilas as $upazila){
            $data .= "<option value='". $upazila->id ."'>" . $upazila->name . "</option>";
        }
        return $data;
    }

    // tracking product from front end by ajax
    // get district by ajax
    public function product_tracking(Request $request)
    {
        $tracking_id = $request->tracking_id;
        if (Order::where('tracking_id', $tracking_id)->exists()){
            $order = Order::where('tracking_id', $tracking_id)->first();
            $data = "Your Order is " . $order->main_order_status->status_title;
        }elseif (OrderDetails::where('tracking_id', $tracking_id)->exists()){
            $order = OrderDetails::where('tracking_id', $tracking_id)->first();
            $data = "Your Product is " . $order->order_status->status_title;
        }else{
            $data = "Sorry! No Data Found";
        }
        return $data;
    }

}
