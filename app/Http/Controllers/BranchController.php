<?php

namespace App\Http\Controllers;

use App\Branch;
use App\District;
use App\Division;
use App\Upazila;
use App\User;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['divisions'] = Division::all();
        $data['managers'] = User::where([
            ['role_id', 2],
            ['approve_status_id', 2],
        ])->get();
        $data['branches'] = Branch::all();
        return view('admin.branch.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'branch_name' => 'required',
           'user_id' => 'required',
           'phone' => 'required',
           'division_id' => 'required',
           'district_id' => 'required',
           'upazila_id' => 'required',
        ]);

        $branch = new Branch();
        $branch->branch_name= $request->branch_name;
        $branch->user_id= $request->user_id;
        $branch->phone= $request->phone;
        $branch->division_id= $request->division_id;
        $branch->district_id= $request->district_id;
        $branch->upazila_id= $request->upazila_id;
        $branch->street= $request->street_address;
        $branch->save();

        User::find($request->user_id)->update([
            'branch_id' => $branch->id
        ]);

        return back()->with('success', 'New Branch Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        $data['divisions'] = Division::all();
        $data['districts'] = District::where('division_id', $branch->division_id)->get();
        $data['upazilas'] = Upazila::where('district_id', $branch->district_id)->get();
        $data['managers'] = User::where([
            ['role_id', 2],
            ['approve_status_id', 2],
        ])->get();
        $data['branch'] = $branch;
        return view('admin.branch.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'branch_name' => 'required',
            'user_id' => 'required',
            'phone' => 'required',
            'division_id' => 'required',
            'district_id' => 'required',
            'upazila_id' => 'required',
        ]);

        $branch = Branch::find($id);
        $branch->branch_name= $request->branch_name;
        $branch->user_id= $request->user_id;
        $branch->phone= $request->phone;
        $branch->division_id= $request->division_id;
        $branch->district_id= $request->district_id;
        $branch->upazila_id= $request->upazila_id;
        $branch->street= $request->street_address;
        $branch->save();

        User::find($request->user_id)->update([
            'branch_id' => $branch->id
        ]);

        return redirect(route('branch.index'))->with('success', 'Branch Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
