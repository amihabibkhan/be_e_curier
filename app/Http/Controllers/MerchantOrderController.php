<?php

namespace App\Http\Controllers;

use App\OrderDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MerchantOrderController extends Controller
{
    // order list view
    public function merchantOrderList($type)
    {
        if ($type == 'all'){
            $data['products'] = OrderDetails::where('merchant_id', Auth::id())->get();
        }
        if ($type == 'cancelled'){

            $data['products'] = OrderDetails::where([
                ['merchant_id', Auth::id()],
                ['order_status_id', 13]
            ])->get();

        }
        if ($type == 'pending-for-return'){

            $data['products'] = OrderDetails::where([
                ['merchant_id', Auth::id()],
                ['order_status_id', 12]
            ])->get();

        }
        if ($type == 'completed'){

            $data['products'] = OrderDetails::where([
                ['merchant_id', Auth::id()],
                ['order_status_id', 7]
            ])->get();

        }
        $data['type'] = $type;
        return view('admin.merchant.orderList', $data);
    }

    // change order status
    public function changeStatusByMerchant($id, $status)
    {
        $order = OrderDetails::find($id);
        if ($order->merchant_id != Auth::id()){
            return back()->with('error', 'Sorry! You have not enough access to do this action');
        }
        $order->order_status_id = $status;
        $order->save();
        return back()->with('success', "Order Status Changed successfully");
    }
}
