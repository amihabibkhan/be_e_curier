<?php

namespace App\Http\Controllers;

use App\IncomeHistory;
use App\PaymentHistory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IncomeController extends Controller
{
    // income history view
    public function boyIncomeHistory()
    {
        $data['incomes'] = IncomeHistory::where('boy_id', Auth::id())->get();
        $data['total_income'] = IncomeHistory::where('boy_id', Auth::id())->sum('amount');
        return view('admin.delivery_boy.incom_and_payment.income', $data);
    }
    
    // boy payment history
    public function boyPaymentHistory()
    {
        $data['payments'] = PaymentHistory::where('boy_id', Auth::id())->get();
        $data['total_payment'] = PaymentHistory::where([
            ['boy_id', Auth::id()],
            ['status', 1]
        ])->sum('amount');
        return view('admin.delivery_boy.incom_and_payment.payment', $data);
    }
    
    // admin delivery history
    public function adminPaymentHistory()
    {
        if (Auth::user()->role_id == 1){

            $data['payments'] = PaymentHistory::all();
            $data['total_payment'] = PaymentHistory::all()->where('status', 1)->sum('amount');
            $data['boys'] = User::where([
                ['role_id', 4],
                ['approve_status_id', 2]
            ])->get();

        }elseif (Auth::user()->role_id == 2){

            $data['payments'] = PaymentHistory::where('paid_by', Auth::id())->get();
            $data['total_payment'] = PaymentHistory::where([
                ['paid_by', Auth::id()],
                ['status', 1]
            ])->sum('amount');
            $data['boys'] = User::where([
                ['role_id', 4],
                ['approve_status_id', 2],
                ['branch_id', Auth::user()->branch_id],
            ])->get();
        }
        return view('admin.paymentHistory', $data);
    }

    // make payment form submit
    public function makePayment(Request $request)
    {
        $request->validate([
           'boy_id' => 'required',
           'amount'=> 'required|numeric',
        ]);

        // check if payment amount is greater then balance
        if ($request->available_balance < $request->amount){
            return back()->with('error', "Insufficient Balance. Please Check");
        }

        PaymentHistory::create([
           'amount' => $request->amount,
           'boy_id' => $request->boy_id,
           'paid_by' => Auth::id(),
           'note' => $request->note,
        ]);

        return back()->with('success', "Payment Completed Successfully");
    }

    // accept payment request by boy
    public function acceptPaymentRequest($type, $id)
    {
        if (PaymentHistory::findOrFail($id)->boy_id != Auth::id()){
            return back()->with('error', "Sorry! You haven't access to do this task");
        }
        if ($type == 'accept'){
            PaymentHistory::findOrFail($id)->update([
               'status' => 1
            ]);
        }else{
            PaymentHistory::findOrFail($id)->update([
                'status' => 2
            ]);
        }

        return back()->with('success', 'Payment Status Changed Successfully');
    }

    // change or delete payment history by admin
    public function changeOrDeletePaymentHistory($type, $id)
    {
        $payment = PaymentHistory::findOrFail($id);
        if ($type == 're-payment'){
            $payment->update([
               'status' => 0
            ]);
        }else{
            if ($payment->status != 1){
                $payment->delete();
            }else{
                return back()->with('error', "Already Completed this payment");
            }
        }
        return back()->with('success', "Payment Status Changed Successfully");
    }


}
