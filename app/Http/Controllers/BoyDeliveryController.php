<?php

namespace App\Http\Controllers;

use App\BoyOrder;
use App\CancelReason;
use App\Notice;
use App\Order;
use App\OrderDetails;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoyDeliveryController extends Controller
{
    // list of delivery product
    public function boyDeliveryList($type)
    {
        if ($type == 'pending'){

            $data['products'] = User::find(Auth::id())->pending_for_delivery;

        }elseif ($type == 'accepted'){

            $data['products'] = User::find(Auth::id())->accepted_delivery_orders;

        }elseif ($type == 'way'){

            $data['products'] = User::find(Auth::id())->on_the_way;

        }elseif ($type == 'complete-pending'){

            $data['products'] = User::find(Auth::id())->complete_pending;

        }elseif ($type == 'completed'){

            $data['products'] = User::find(Auth::id())->completed_delivery_orders;

        }elseif ($type == 'rejected'){

            $data['products'] = User::find(Auth::id())->rejected_delivery_orders;

        }elseif ($type == 'cancelled'){

            $data['products'] = User::find(Auth::id())->cancelled_delivery_orders;

        }
        $data['type'] = $type;
        $data['cancels'] = CancelReason::all();
        return view('admin.delivery_boy.delivery.deliveryOrderList', $data);
    }

    // accept delivery request
    public function acceptDeliveryRequest($id)
    {
        BoyOrder::where([
            ['order_id', $id],
            ['user_id', Auth::id()],
            ['boy_order_status_id', 1]
        ])->update([
            'boy_order_status_id' => 2
        ]);

        return back()->with('success', "Request Accepted Successfully");
    }

    // reject delivery request
    public function rejectDeliveryRequest($id)
    {
        BoyOrder::where([
            ['order_id', $id],
            ['user_id', Auth::id()],
            ['boy_order_status_id', 1]
        ])->update([
            'boy_order_status_id' => 3
        ]);

        OrderDetails::find($id)->update([
           'order_status_id' => 4
        ]);

        return back()->with('success', "Request Rejected Successfully");
    }

    // collect product
    public function collectProductForDelivery($id)
    {
        BoyOrder::where([
            ['order_id', $id],
            ['user_id', Auth::id()],
            ['boy_order_status_id', 2]
        ])->update([
            'boy_order_status_id' => 6
        ]);

        OrderDetails::find($id)->update([
           'order_status_id' => 6
        ]);


        $order_details = OrderDetails::find($id);
        // notification
        Notice::insert([
            'type' => 'Product is on the way',
            'details' => 'Your product (' . OrderDetails::find($id)->tracking_id . ') is on the way to deliver',
            'user_id' => Order::find(OrderDetails::find($id)->order_id)->user_id,
            'created_at' => Carbon::now()
        ]);

        // sms to client

        $date = date_format(date_create(Carbon::now()->addDays(1)), "d F");
        $url = "http://66.45.237.70/api.php";
        $number="{$order_details->receiver_phone}";
        $text="Dear " . $order_details->receiver_name . ",
Your product has been taken by " . Auth::user()->name . ". Expected day of arrival on " . $date . ". If you are not home at that day, please contact (" . Auth::user()->phone. ")

Thank You.";
        $data= array(
            'username'=>"01712794033",
            'password'=>"strong@787664",
            'number'=>"$number",
            'message'=>"$text"
        );

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $smsresult = curl_exec($ch);
        $p = explode("|",$smsresult);
        $sendstatus = $p[0];

        return back()->with('success', "Product Collected Successfully");
    }

    // complete delivery request
    public function requestToCompleteDelivery($id)
    {
        BoyOrder::where([
            ['order_id', $id],
            ['user_id', Auth::id()],
            ['boy_order_status_id', 6]
        ])->update([
            'boy_order_status_id' => 7
        ]);

        OrderDetails::find($id)->update([
            'order_status_id' => 8
        ]);

        return back()->with('success', "Product Delivered Successfully");
    }

    // cancel delivery boy boy
    public function cancelOrderByBoy(Request $request)
    {
        $product = OrderDetails::find($request->order_id);

        // check this order is related with the boy and its ready to cancel step
        if (BoyOrder::where([['user_id', Auth::id()], ['order_id', $request->order_id]])->first()->boy_order_status_id == 6){
            // change product status
            $product->order_status_id = 9;
            $product->cancel_reason_id = $request->reason_id;
            $product->save();

            // change boy order table status
            BoyOrder::where([['user_id', Auth::id()], ['order_id', $request->order_id]])->first()->update([
               'boy_order_status_id' => 4,
            ]);

            return back()->with('success', 'Order Cancelled Successfully');
        }
        return back()->with('error', "You are not permitted to do this action");
    }
}
