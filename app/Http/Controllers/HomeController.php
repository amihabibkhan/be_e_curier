<?php

namespace App\Http\Controllers;

use App\Area;
use App\BoyOrder;
use App\Branch;
use App\DeliverySystem;
use App\District;
use App\Division;
use App\MerchantFee;
use App\MerchantPayment;
use App\Notice;
use App\Order;
use App\OrderDetails;
use App\PickerAssign;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Upazila;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role_id == 1) {

            // chart info get month name
            $data['months'] = "[";
            for ($i = 1; $i <= 12; $i++) {
                $data['months'] .= "'". date("F", strtotime( date( 'Y-m-01' )." + $i months")) . "',";
            }
            $data['months'] .= "]";

            // get month wise data
            $data['month_data'] = "[";
            for ($i = 11; $i >= 0; $i--) {
                $month = date("Y-m", strtotime( date( 'Y-m-01' )." - $i months"));
                $data['month_data'] .= "'". OrderDetails::where('created_at', 'like', $month . "%")->count() . "',";
            }

            $data['month_data'] .= "]";









            // merchant list
            $data['active_merchant'] = User::where(['role_id' => 3, 'approve_status_id' => 2])->count();
            $data['pending_merchant'] = User::where(['role_id' => 3, 'approve_status_id' => 1])->count();
            $data['suspend_merchant'] = User::where(['role_id' => 3, 'approve_status_id' => 3])->count();
            // delivery boy list
            $data['active_boy'] = User::where(['role_id' => 4, 'approve_status_id' => 2])->count();
            $data['pending_boy'] = User::where(['role_id' => 4, 'approve_status_id' => 1])->count();
            $data['suspend_boy'] = User::where(['role_id' => 4, 'approve_status_id' => 3])->count();
            // pick order list
            $data['total_order'] = Order::count();
            $data['total_products'] = OrderDetails::count();
            $data['delivery_completed'] = OrderDetails::where('order_status_id', 7)->count();
            $data['delivery_canceled'] = OrderDetails::where('order_status_id', 13)->count();
            $data['delivery_processing'] = OrderDetails::where([['order_status_id', '!=' , 13], ['order_status_id', '!=' , 7]])->count();
            // product price and payment
            $data['total_product_price'] = OrderDetails::sum('total_amount');
            $data['paid_to_merchant'] = MerchantPayment::where('status', 1)->sum('amount');
            // product delivery fee
            $data['total_delivery_fee'] = OrderDetails::sum('delivery_fee');
            $data['completed_delivery_fee'] = MerchantFee::where( 'status', 1)->sum('amount');
            // branch
            $data['total_branch'] = Branch::count();
            // notification
            $data['notification'] = Notice::where(['user_id' => NULL, 'read_status_admin' => 0])->count();
            // delivery system
            $data['delivery_system'] = DeliverySystem::count();

            return view('admin.home', $data);
        }else{
            return redirect(route('personalProfile'));
        }
    }

    // personal profile view
    public function personalProfile()
    {
        $data = [];
        // running user info
        $user = User::find(Auth::id());

        if ($user->role_id == 4) {
            // delivery boy
            $data['boy_balance'] = $user->boy_income() - $user->boy_payment();
            $data['boy_total_income'] = $user->boy_income();
            $data['boy_paid'] = $user->boy_payment();
            $data['boy_order_completed'] = BoyOrder::where([['user_id', Auth::id()], ['boy_order_status_id', 5]])->count();
            $data['boy_order_rejected'] = BoyOrder::where([['user_id', Auth::id()], ['boy_order_status_id', 3]])->count();
            $data['boy_delivery_pending'] = BoyOrder::where([['user_id', Auth::id()], ['boy_order_status_id', 1]])->count();
            $data['boy_picking_pending'] = PickerAssign::where([['picker', Auth::id()], ['boy_order_status_id', 1]])->count();
        }
        // merchant
        if ($user->role_id == 3) {
            $data['merchant_total_product_price'] = $user->totalAmountOfMerchant();
            $data['merchant_received'] = $user->merchantPaid();
            $data['merchant_delivery_fee'] = $user->merchantTotalDeliveryFee();
            $data['merchant_delivery_fee_paid'] = $user->merchantDeliveryFeePaid();
            $data['merchant_total_order'] = OrderDetails::where('merchant_id', Auth::id())->count();
            $data['merchant_delivery_completed'] = OrderDetails::where([['merchant_id', Auth::id()], ['order_status_id', 7]])->count();
            $data['merchant_canceled_order'] = OrderDetails::where([['merchant_id', Auth::id()], ['order_status_id', 13]])->count();
            $data['merchant_processing_order'] = OrderDetails::where([['merchant_id', Auth::id()], ['order_status_id', '!=' , 13], ['order_status_id', '!=' , 7]])->count();
        }

        // manager
        if ($user->role_id == 2){
            // merchant list
            $data['manager_total_merchant_active'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 3, 'approve_status_id' => 2])->count();
            $data['manager_total_merchant_pending'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 3, 'approve_status_id' => 1])->count();
            $data['manager_total_merchant_suspended'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 3, 'approve_status_id' => 3])->count();
            // delivery boy list
            $data['manager_total_delivery_active'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 4, 'approve_status_id' => 2])->count();
            $data['manager_total_delivery_pending'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 4, 'approve_status_id' => 1])->count();
            $data['manager_total_delivery_suspended'] = User::where(['branch_id' => $user->branch_id, 'role_id' => 4, 'approve_status_id' => 3])->count();
            // pick order list
            $data['manger_total_order'] = OrderDetails::where('from_branch', $user->branch_id)->count();
            $data['manger_delivery_completed'] = OrderDetails::where([['from_branch', $user->branch_id], ['order_status_id', 7]])->count();
            $data['manger_canceled_order'] = OrderDetails::where([['from_branch', $user->branch_id], ['order_status_id', 13]])->count();
            $data['manger_processing_order'] = OrderDetails::where([['from_branch', $user->branch_id], ['order_status_id', '!=' , 13], ['order_status_id', '!=' , 7]])->count();
            // delivery order list
            $data['manger_total_order_delivery'] = OrderDetails::where('to_branch', $user->branch_id)->count();
            $data['manger_delivery_completed_delivery'] = OrderDetails::where([['to_branch', $user->branch_id], ['order_status_id', 7]])->count();
            $data['manger_canceled_order_delivery'] = OrderDetails::where([['to_branch', $user->branch_id], ['order_status_id', 13]])->count();
            $data['manger_processing_order_delivery'] = OrderDetails::where([['to_branch', $user->branch_id], ['order_status_id', '!=' , 13], ['order_status_id', '!=' , 7]])->count();
            // product price and payment
            $data['manger_total_product_price'] = OrderDetails::where('from_branch', $user->branch_id)->sum('total_amount');
            $data['manger_paid_to_merchant'] = MerchantPayment::where(['paid_by' => $user->id, 'status' => 1])->sum('amount');
            // product delivery fee
            $data['manger_total_delivery_fee'] = OrderDetails::where('from_branch', $user->branch_id)->sum('delivery_fee');
            $data['manger_completed_delivery_fee'] = MerchantFee::where(['paid_to' => $user->id, 'status' => 1])->sum('amount');
        }

        return view('admin.personal_profile', $data);
    }

    // update profile form page view
    public function updateProfile()
    {
        $data['divisions'] = Division::all();
        $data['districts'] = District::where('division_id', auth::user()->division_id)->get();
        $data['upazilas'] = Upazila::where('district_id', auth::user()->district_id)->get();
        $data['branches'] = Branch::all();
        $data['areas'] = Area::all();
        return view('admin.updateProfile', $data);
    }
}
