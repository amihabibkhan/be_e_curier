<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApproveStatus extends Model
{
    protected $guarded = [];
}
