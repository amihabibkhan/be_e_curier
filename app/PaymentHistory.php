<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $guarded = [];

    // relation with user table for paid by
    public function user()
    {
        return $this->belongsTo(User::class, 'paid_by');
    }
}
