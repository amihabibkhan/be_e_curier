<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeHistory extends Model
{
    protected $guarded = [];

    // relation with order details table
    public function order_detail()
    {
        return $this->belongsTo(OrderDetails::class);
    }

    // relation with order type table
    public function order_type()
    {
        return $this->belongsTo(OrderType::class);
    }
}
