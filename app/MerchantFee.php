<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantFee extends Model
{
    protected $guarded = [];

    // relation with user table for paid by
    public function get_paid_to()
    {
        return $this->belongsTo(User::class, 'paid_to');
    }
    // relation with user table for paid to means merchant
    public function get_merchant_info()
    {
        return $this->belongsTo(User::class, 'merchant_id');
    }
}
