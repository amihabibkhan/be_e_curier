<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryPaymentSystem extends Model
{
    protected $guarded = [];
}
