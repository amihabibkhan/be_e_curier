<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    protected $guarded = [];
    
    // relation with status table
    public function main_order_status()
    {
        return $this->belongsTo(MainOrderStatus::class);
    }

    // relation with user table
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // relation with picker assign table
    public function picker_assign()
    {
        return $this->hasOne('App\PickerAssign')->where('picker', Auth::id());
    }

    // has many to order details
    public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'order_id', 'id');
    }
}
