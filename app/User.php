<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // relation with branch table
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    // relation with branch table
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    // approve status relation
    public function approve_status()
    {
        return $this->belongsTo(ApproveStatus::class);
    }

    // division table relation
    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    // district table relation
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    // upazila table relation
    public function upazila()
    {
        return $this->belongsTo(Upazila::class);
    }

    // area table relation
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    // many to many relation with picker assign (pending)
    public function pending_pick_orders()
    {
        return $this->belongsToMany('App\Order', 'picker_assigns', 'picker', 'order_id')->where('boy_order_status_id', 1);
    }
    // many to many relation with picker assign (accepted)
    public function accepted_pick_orders()
    {
        return $this->belongsToMany('App\Order', 'picker_assigns', 'picker', 'order_id')->where('boy_order_status_id', 2);
    }
    // many to many relation with picker assign (rejected)
    public function rejected_pick_orders()
    {
        return $this->belongsToMany('App\Order', 'picker_assigns', 'picker', 'order_id')->where('boy_order_status_id', 3);
    }
    // many to many relation with picker assign (completed)
    public function completed_pick_orders()
    {
        return $this->belongsToMany('App\Order', 'picker_assigns', 'picker', 'order_id')->where('boy_order_status_id', 5);
    }


    // delivery orders
    public function pending_for_delivery()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 1);
    }
    // accepted delivery orders
    public function accepted_delivery_orders()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 2);
    }
    // rejected delivery orders
    public function rejected_delivery_orders()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 3);
    }
    // on the way for delivery
    public function on_the_way()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 6);
    }
    // complete pending for review
    public function complete_pending()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 7);
    }
    // completed order list
    public function completed_delivery_orders()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 5);
    }
    // Cancelled order list
    public function cancelled_delivery_orders()
    {
        return $this->belongsToMany(OrderDetails::class, 'boy_orders', 'user_id', 'order_id')->where('boy_order_status_id', 4);
    }



    // get delivery boy total income
    public function boy_income()
    {
        return $this->hasMany(IncomeHistory::class, 'boy_id', 'id')->sum('amount');
    }
    // get delivery boy total income
    public function boy_payment()
    {
        return $this->hasMany(PaymentHistory::class, 'boy_id', 'id')->where('status', 1)->sum('amount');
    }


    // merchant total amount of delivery fee
    public function totalAmountOfMerchant()
    {
        return $this->hasMany(OrderDetails::class, 'merchant_id', 'id')->sum('total_amount');
    }

    // merchant total amount of payment
    public function merchantPaid()
    {
        return $this->hasMany(MerchantPayment::class, 'merchant_id', 'id')->where('status', 1)->sum('amount');
    }



    // merchant total amount of delivery fee
    public function merchantTotalDeliveryFee()
    {
        return $this->hasMany(OrderDetails::class, 'merchant_id', 'id')->sum('delivery_fee');
    }

    // merchant total delivery fee completed
    public function merchantDeliveryFeePaid()
    {
        return $this->hasMany(MerchantFee::class, 'merchant_id', 'id')->where('status', 1)->sum('amount');
    }
}
