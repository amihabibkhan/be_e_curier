<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $guarded = [''];

    // division table relation
    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    // district table relation
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    // upazila table relation
    public function upazila()
    {
        return $this->belongsTo(Upazila::class);
    }

    // user table relation for branch manager
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
