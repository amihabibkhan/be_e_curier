<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // for user role table
        DB::table('roles')->insert([
            ['role' => "Admin"],
            ['role' => "Branch Manager"],
            ['role' => "Merchant"],
            ['role' => "Delivery Boy"],
        ]);

        // approve status table
        DB::table('approve_statuses')->insert([
            ['status_title' => 'Pending'],
            ['status_title' => 'Approved'],
            ['status_title' => 'Suspended'],
        ]);

        // Main order status
        DB::table('main_order_statuses')->insert([
           ['status_title' => 'Request Pending'],
           ['status_title' => 'Approved By Admin'],
           ['status_title' => 'Product Collected'],
           ['status_title' => 'Processing for Delivery'],
           ['status_title' => 'Delivery Completed'],
        ]);

        // Order status
        DB::table('order_statuses')->insert([
           ['status_title' => 'Collected'],
           ['status_title' => 'Arrived to Office'],
           ['status_title' => 'Sent to related branch'],
           ['status_title' => 'Arrived to related branch'],
           ['status_title' => 'Taken by Delivery Boy'],
           ['status_title' => 'On the way'],
           ['status_title' => 'Delivery Completed'],
           ['status_title' => 'Requested for Complete'],
           ['status_title' => 'Cancelled'],
           ['status_title' => 'Requested to Returned'],
           ['status_title' => 'Returned to Office'],
           ['status_title' => 'Sent to Merchant (Approval Pending)'],
           ['status_title' => 'Received by Merchant'],
        ]);

        // Boy order Status
        DB::table('boy_order_statuses')->insert([
           ['status_title' => 'Pending'],
           ['status_title' => 'Accepted'],
           ['status_title' => 'Rejected'],
           ['status_title' => 'Cancelled'],
           ['status_title' => 'Completed'],
           ['status_title' => 'Product Collected'],
        ]);

        // Delivery Payment System
        DB::table('delivery_payment_systems')->insert([
           ['system_title' => 'Cash On Delivery'],
           ['system_title' => 'Prepaid Delivery'],
        ]);

        // Order type table
        DB::table('order_types')->insert([
           ['type_title' => 'Order Receiving'],
           ['type_title' => 'Order Delivery'],
        ]);
    }
}
