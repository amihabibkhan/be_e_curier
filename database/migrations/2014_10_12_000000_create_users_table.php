<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('role_id');
            $table->integer('branch_id');
            $table->string('phone');
            $table->string('profile_pic')->nullable();
            $table->integer('approve_status_id')->default(1);
            $table->string('designation')->nullable();
            $table->string('company_name')->nullable();
            $table->string('fb_fan_page')->nullable();
            $table->string('website')->nullable();
            $table->string('company_logo')->nullable();
            $table->string('division_id')->nullable();
            $table->string('district_id')->nullable();
            $table->string('upazila_id')->nullable();
            $table->string('area_id')->nullable();
            $table->string('nid_image')->nullable();
            $table->string('nid_number')->nullable();
            $table->string('working_status')->default(1);
            $table->string('parmanent_address')->nullable();
            $table->string('present_address')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
