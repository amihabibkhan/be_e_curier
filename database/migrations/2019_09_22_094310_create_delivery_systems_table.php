<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverySystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('system_details');
            $table->integer('receiver_boy_fee');
            $table->integer('delivery_boy_fee');
            $table->integer('company_fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_systems');
    }
}
