<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('merchant_id');
            $table->integer('from_branch');
            $table->integer('to_branch');
            $table->text('product_description')->nullable();
            $table->string('receiver_name');
            $table->string('receiver_phone');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->integer('upazila_id');
            $table->integer('area_id')->nullable();
            $table->integer('order_status_id')->default(1);
            $table->integer('total_amount')->default(0);
            $table->integer('delivery_fee')->default(0);
            $table->integer('delivery_system_id');
            $table->integer('delivery_fee_payment_status')->default(0);
            $table->integer('delivery_payment_system_id')->default(1);
            $table->string('tracking_id');
            $table->integer('cancel_reason_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
